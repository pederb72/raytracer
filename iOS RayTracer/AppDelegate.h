//
//  AppDelegate.h
//  iOS RayTracer
//
//  Created by Peder Blekken on 7/4/18.
//  Copyright © 2018 Peder Blekken. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

