//
//  Renderer.mm
//  iOS RayTracer
//
//  Created by Peder Blekken on 7/4/18.
//  Copyright © 2018 Peder Blekken. All rights reserved.
//

#import <simd/simd.h>
#import "Renderer.h"
#import "MetalRayTracer.h"
#include "RandomGen.hpp"
#include "Scene.hpp"
#include "Camera.hpp"
#include "World.hpp"
#include "Import_minilight.hpp"

using namespace IvES::GI;

@interface Renderer ()
@property (nonatomic, copy) NSString *currentFile;
@end

@implementation Renderer
{
    id <MTLDevice> _device;
    MetalRayTracer *_metalRayTracer;
    
    std::shared_ptr<World> _world;
    int _frameNo;
    RandomGen _random;
    BOOL _cancelled;
}

-(nonnull instancetype)initWithMetalKitView:(nonnull MTKView *)view;
{
    self = [super init];
    if(self)
    {
        _device = view.device;
        view.colorPixelFormat = MTLPixelFormatBGRA8Unorm_sRGB;
        view.sampleCount = 1;
        view.enableSetNeedsDisplay = YES;
        view.delegate = self;
        view.framebufferOnly = NO;
    }

    return self;
}

- (void)loadFile:(NSString *)file size:(CGSize)size
{
    self.currentFile = file;
    const std::string modelFilePathname(file.UTF8String);
    _world = IvES::GI::import_minilight(modelFilePathname);
    _world.get()->updateImageSize(size.width, size.height);
    _metalRayTracer = [[MetalRayTracer alloc] initWithWorld:_world device:_device imageMode:NO];
}

- (void)drawInMTKView:(nonnull MTKView *)view
{
    [_metalRayTracer renderFrame];
    [_metalRayTracer copyToDrawable:view.currentDrawable];
    dispatch_async(dispatch_get_main_queue(), ^{
        [view setNeedsDisplay];
    });
}

- (void)mtkView:(nonnull MTKView *)view drawableSizeWillChange:(CGSize)size
{
    NSBundle *bundle = [NSBundle mainBundle];
    NSURL *url = [bundle URLForResource:@"cornellbox-n.ml" withExtension:@"txt"];
    NSString *filename = [url path];
    [self loadFile:filename size:view.drawableSize];
    [view setNeedsDisplay];
}

@end
