//
//  Renderer.h
//  iOS RayTracer
//
//  Created by Peder Blekken on 7/4/18.
//  Copyright © 2018 Peder Blekken. All rights reserved.
//

#import <MetalKit/MetalKit.h>

// Our platform independent renderer class.   Implements the MTKViewDelegate protocol which
//   allows it to accept per-frame update and drawable resize callbacks.
@interface Renderer : NSObject <MTKViewDelegate>

-(nonnull instancetype)initWithMetalKitView:(nonnull MTKView *)view;

@end

