//
//  GameViewController.h
//  iOS RayTracer
//
//  Created by Peder Blekken on 7/4/18.
//  Copyright © 2018 Peder Blekken. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Metal/Metal.h>
#import <MetalKit/MetalKit.h>
#import "Renderer.h"

// Our iOS view controller
@interface GameViewController : UIViewController

@end
