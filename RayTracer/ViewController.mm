//
//  ViewController.mm
//  RayTracer
//
//  Created by Peder Blekken on 5/20/18.
//  Copyright © 2018 Peder Blekken. All rights reserved.
//

#import "ViewController.h"
#include <string>

#include <signal.h>
#include <math.h>
//#include <time.h>
#include <iostream>
#include <fstream>
#include <string>
#include <stdexcept>
#include <cstdlib>
#include <set>

#include "RandomGen.hpp"
#include "Image.hpp"
#include "Scene.hpp"
#include "Camera.hpp"
#include "Image_tonemap.hpp"
#include "Material.hpp"
#include "Mesh.hpp"
#include "Import_minilight.hpp"
#include "World.hpp"
#include "IndirectRayTracer.hpp"
#import "MetalRayTracer.h"

#import <Metal/Metal.h>

// #import <MetalPerformanceShaders/MetalPerformanceShaders.h>

#define USE_CPU 0

using namespace IvES::GI;

@interface MyRootView : NSView
// should have been a delegate but I'm lazy
@property (nonatomic, weak) ViewController *controller;
@end

@interface MyImageView : NSImageView
// should have been a delegate but I'm lazy
@property (nonatomic, weak) ViewController *controller;
@end

@interface ViewController ()
@property (nonatomic, strong) IBOutlet MyImageView *imageView;
@property (nonatomic, strong) IBOutlet NSTextField *label;
@property (nonatomic, copy) NSString *currentFile;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *widthConstraint;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *heightConstraint;
@end

@implementation ViewController {
    id<MTLDevice> _device;
    std::shared_ptr<World> _world;
    std::shared_ptr<Image<Vec3>> _image;
    int _frameNo;
    RandomGen _random;
    NSOperationQueue *_workQueue;
    BOOL _cancelled;
    MetalRayTracer *_metalRayTracer;
}

- (void)viewDidLoad
{
    NSLog(@"sizeof vec3: %lu", sizeof(Vec3));
    [super viewDidLoad];
    _device = MTLCreateSystemDefaultDevice();
    _workQueue = [[NSOperationQueue alloc] init];
    _workQueue.maxConcurrentOperationCount = 1;
    _workQueue.qualityOfService = NSQualityOfServiceBackground;
    self.imageView.controller = self;
    
    ((MyRootView *)self.view).controller = self;
    // Do any additional setup after loading the view.
}

- (void)viewDidDisappear
{
    _cancelled = YES;
    [_workQueue cancelAllOperations];
    
    [super viewDidDisappear];
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

- (void)viewDidAppear
{
    [super viewDidAppear];
    [self updateImageView];
}

- (BOOL)updateImageView
{
    CGFloat size = MIN(self.view.bounds.size.width, self.view.bounds.size.height);
    BOOL didChange = NO;
    if (self.widthConstraint.constant != size) {
        self.widthConstraint.constant = size;
        didChange = YES;
    }
    if (self.heightConstraint.constant != size) {
        self.heightConstraint.constant = size;
        didChange = YES;
    }
    
    if (didChange && self.currentFile) {
        [self loadFile:self.currentFile];
    }
    
    return didChange;
}

- (void)viewDidLayout
{
    [super viewDidLayout];
    if (!self.view.inLiveResize) {
        [self updateImageView];
    }
}

+ (CGImageRef)CGImageFromImage:(const Image<Pixel> &)image CF_RETURNS_RETAINED
{
    int width = image.size().x;
    int height = image.size().y;
    int numcomponents = 4;
    
    unsigned int imgbufsize = width*height*numcomponents;

    unsigned char *bytes_flipped = (unsigned char *)malloc(imgbufsize);
    unsigned char *dst = bytes_flipped;

    for (int y = height-1; y >= 0; y--) {
        const unsigned char *src = (const unsigned char *)&image.pixel(MakeUInt2(0, y));
        memcpy(dst, src, width*numcomponents);
        dst += numcomponents * width;
    }
    
    CGImageRef image_source;
    CGColorSpaceRef color_space;

    CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, bytes_flipped, imgbufsize, NULL);
    color_space = CGColorSpaceCreateDeviceRGB();
    image_source = CGImageCreate(width, height, 8, 8*numcomponents,
                                 numcomponents*width,
                                 color_space,
                                 kCGBitmapByteOrderDefault|kCGImageAlphaNoneSkipLast,
                                 provider,
                                 NULL, 0,
                                 kCGRenderingIntentDefault);
    
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(color_space);
    free(bytes_flipped);
    
    return image_source;
}

- (void)_updateImage
{
#if !USE_CPU
    [_metalRayTracer copyToImage:*_image.get()];
    CGImageRef cgImage = [_metalRayTracer newCGImage];
#else
    Image<Pixel> image;
    IvES::GI::tonemap(*_image.get(), image);
    CGImageRef cgImage = [[self class] CGImageFromImage:image];
#endif
    NSImage *nsImage = [[NSImage alloc] initWithCGImage:cgImage size:NSMakeSize(_image.get()->size()[0], _image.get()->size()[1])];
    CGImageRelease(cgImage);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.imageView.image = nsImage;
    });
}

- (void)_iterate
{
    if (_cancelled) {
        return;
    }
#if USE_CPU
    const Camera & camera = _world.get()->camera();
    const Scene & scene = _world.get()->scene();
    render_frame(scene, camera, _random, *_image.get(), _frameNo);
    _frameNo++;
#else
    const int numPasses = 1;
    for (int i = 0; i < numPasses; i++) {
        _frameNo++;
        [_metalRayTracer renderFrame];
        [_metalRayTracer updateImage];
    }
#endif
 
    if (!_cancelled) {
        [self _updateImage];
    
        const int iterations = _world.get()->iterations();
        dispatch_async(dispatch_get_main_queue(), ^{
            self.label.stringValue = [NSString stringWithFormat:@"%lu/%lu", (unsigned long)self->_frameNo, (unsigned long)iterations];

            if (!self->_cancelled && self->_frameNo < iterations) {
                [self->_workQueue addOperationWithBlock:^{
                    [self _iterate];
                }];
            }
        });
    }
}

- (void)loadFile:(NSString *)file
{
    self.currentFile = file;

    _cancelled = YES;
    [_workQueue waitUntilAllOperationsAreFinished];
    _cancelled = NO;
    _frameNo = 0;

    const std::string modelFilePathname(file.UTF8String);
    _world = IvES::GI::import_minilight(modelFilePathname);
    
    CGSize size = self.imageView.bounds.size;
    _world.get()->updateImageSize(size.width, size.height);
    
    _image.reset(new Image<Vec3>(_world.get()->imageSize()));
    memset(&_image.get()->pixels()[0], 0, sizeof(Vec3)*_image.get()->pixels().size());
    
    _metalRayTracer = [[MetalRayTracer alloc] initWithWorld:_world device:_device imageMode:YES];
    
    [_workQueue addOperationWithBlock:^{
        [self _iterate];
    }];
}

@end

#pragma mark - root view

@implementation MyRootView

- (NSDragOperation)draggingEntered:(id<NSDraggingInfo>)sender
{
    NSPasteboard *pboard = [sender draggingPasteboard];
    
    if ([[pboard types] containsObject:NSFilenamesPboardType])
    {
        return NSDragOperationGeneric;
    }
    return NSDragOperationNone;
}

- (BOOL)performDragOperation:(id<NSDraggingInfo>)sender
{
    NSPasteboard *pboard = [sender draggingPasteboard];
    
    if ([[pboard types] containsObject:NSFilenamesPboardType])
    {
        NSArray *files = [pboard propertyListForType:NSFilenamesPboardType];
        [self.controller loadFile:files[0]];
        return YES;
    }
    return NO;
}

- (void)viewWillStartLiveResize
{
}

- (void)viewDidEndLiveResize
{
    [self.controller updateImageView];
}

@end

#pragma mark - ImageView

@implementation MyImageView

- (NSDragOperation)draggingEntered:(id<NSDraggingInfo>)sender
{
    NSPasteboard *pboard = [sender draggingPasteboard];
    
    if ([[pboard types] containsObject:NSFilenamesPboardType])
    {
        return NSDragOperationGeneric;
    }
    return NSDragOperationNone;
}

- (BOOL)performDragOperation:(id<NSDraggingInfo>)sender
{
    NSPasteboard *pboard = [sender draggingPasteboard];
    
    if ([[pboard types] containsObject:NSFilenamesPboardType])
    {
        NSArray *files = [pboard propertyListForType:NSFilenamesPboardType];
        [self.controller loadFile:files[0]];
        return YES;
    }
    return NO;
}

@end
