//
//  AppDelegate.m
//  RayTracer
//
//  Created by Peder Blekken on 5/20/18.
//  Copyright © 2018 Peder Blekken. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


@end
