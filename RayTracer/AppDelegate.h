//
//  AppDelegate.h
//  RayTracer
//
//  Created by Peder Blekken on 5/20/18.
//  Copyright © 2018 Peder Blekken. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

