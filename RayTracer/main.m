//
//  main.m
//  RayTracer
//
//  Created by Peder Blekken on 5/20/18.
//  Copyright © 2018 Peder Blekken. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
