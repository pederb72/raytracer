//
//  IvMetalFramebuffer.m 
//
//  Created by Peder Blekken on 10/24/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "MetalFramebuffer.h"
#import <IOKit/IOTypes.h>

@implementation IvMetalFramebuffer {
#if !TARGET_OS_SIMULATOR
    IOSurfaceRef _ioSurface;
#endif
}

- (instancetype)init
{
    @throw @"Not available";
}

- (instancetype)initWithSize:(CGSize)size pixelFormat:(MTLPixelFormat)pixelFormat device:(id<MTLDevice>)device memoryless:(BOOL)memoryless backedByIOSurface:(BOOL)backedByIOSurface sampleCount:(NSUInteger)sampleCount
{
    self = [super init];
    if (self) {
        _size = CGSizeMake(ceil(size.width), ceil(size.height));
        _device = device;
        _pixelFormat = pixelFormat;
        _memoryless = memoryless;
        _backedByIOSurface = backedByIOSurface;
        _sampleCount = sampleCount;
        [self createColorTextureIfNecessary];
    }
    
    return self;
}

- (void)dealloc
{
    if (_pixelBuffer) {
        CVPixelBufferRelease(_pixelBuffer);
        _pixelBuffer = nil;
    }
#if !TARGET_OS_SIMULATOR
    if (_ioSurface) {
        CFRelease(_ioSurface);
        _ioSurface = nil;
    }
#endif
}

- (void)createColorTextureIfNecessary
{
    if (!_colorTexture) {
        // TODO: should be possible to specify type and options
        MTLTextureDescriptor *desc = [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:self.pixelFormat
                                                                                        width:_size.width
                                                                                       height:_size.height
                                                                                    mipmapped:NO];
        desc.textureType = _sampleCount > 1 ? MTLTextureType2DMultisample : MTLTextureType2D;
        desc.sampleCount = _sampleCount;
        // desc.usage = MTLTextureUsageRenderTarget;
        desc.usage = MTLTextureUsageShaderWrite | MTLTextureUsageShaderRead | MTLTextureUsageRenderTarget;
        if (!_memoryless) {
            desc.usage |= MTLTextureUsageShaderRead;
        }
        desc.storageMode = MTLStorageModePrivate;
#if TARGET_OS_IPHONE
        if (self.memoryless) {
            desc.storageMode = MTLStorageModeMemoryless;
        }
#endif
        if (_backedByIOSurface) {
#if !TARGET_OS_SIMULATOR
            _ioSurface = [self createIOSurface];
            CVPixelBufferCreateWithIOSurface(kCFAllocatorDefault, _ioSurface, NULL, &_pixelBuffer);
            _colorTexture = [_device newTextureWithDescriptor:desc iosurface:_ioSurface plane:0];
#endif
        }
        else {
            _colorTexture = [_device newTextureWithDescriptor:desc];
        }
    }
}

#if !TARGET_OS_SIMULATOR
- (IOSurfaceRef)createIOSurface
{
    // IOSurface bytesPerRow need to be a multiple of 16 for certain devices (N66, J2, etc)
    // Also required by ME
    static const CGFloat kHardwareStrideLength = 16.0;
    CGFloat width = floor(_size.width);
    CGFloat height = floor(_size.height);
    NSUInteger bytesPerRow = ceil(width*4 / kHardwareStrideLength) * kHardwareStrideLength;

    NSUInteger rows = height;
    NSUInteger length = bytesPerRow * rows;
        
    NSDictionary *properties = @{(id)kIOSurfaceWidth : @(width),
                                 (id)kIOSurfaceHeight : @(height),
                                 (id)kIOSurfacePixelFormat : @((int)'BGRA'),
                                 (id)kIOSurfaceBytesPerElement : @4,
                                 (id)kIOSurfaceBytesPerRow : @(bytesPerRow),
                                 (id)kIOSurfaceAllocSize : @(length),
                                 (id)kIOSurfaceCacheMode : @(kIOMapDefaultCache)};
        
    IOSurfaceRef surface = IOSurfaceCreate((CFDictionaryRef)properties);
    return surface;
}
#endif

- (BOOL)makeNonPurgeable
{
    if (!self.memoryless) {
        return YES;
    }
    
    return [self.colorTexture setPurgeableState:MTLPurgeableStateNonVolatile] != MTLPurgeableStateEmpty;
}

- (void)makePurgeable
{
    if (!self.memoryless) {
        [self.colorTexture setPurgeableState:MTLPurgeableStateVolatile];
    }
}

@end
