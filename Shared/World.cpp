#include "World.hpp"

using namespace IvES::GI;

World::World(std::shared_ptr<Mesh> mesh,
             const Camera & camera,
             const Vec3 & skyEmission,
             const Vec3 & groundReflection,
             int iterations,
             const UInt2 & imageSize)

    : mMesh(mesh),
      mCamera(camera),
      mIterations(iterations),
      mImageSize(imageSize),
      mScene(mMesh.get()->triangles(),
             mMesh.get()->materials(),
             camera.viewPosition,
             skyEmission,
             groundReflection)
{
    mAspectRatio = float(imageSize[0]) / float(imageSize[1]);
}

void World::updateImageSize(unsigned int width, unsigned int height)
{
    unsigned int newWidth = ceil(height * mAspectRatio);
    if (newWidth > width) {
        height = ceil(width / mAspectRatio);
    }
    else {
        width = newWidth;
    }
    
    mImageSize = MakeUInt2(width, height);
}
