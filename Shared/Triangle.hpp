#ifndef Triangle_h
#define Triangle_h

#include "Basic.hpp"
#include "Ray.hpp"

IVES_GI_NAMESPACE_BEGIN

struct RandomGen;
struct Box3;

struct Triangle {
    Vec3 v0, v1, v2;
    Vec3 normal;
    Vec3 tangent;
    Float area;
    unsigned int materialIndex;
};

#if !__METAL_VERSION__

Triangle make_triangle(const Vec3 & v0,
                       const Vec3 & v1,
                       const Vec3 & v2,
                       unsigned int materialIndex);

#endif

bool getIntersection(const IvDevice Triangle & triangle,
                     Ray ray,
                     IvThread Float & hitDistance);

Vec3 getSamplePoint(const IvDevice Triangle & triangle,
                    IvThread RandomGen & random);

Vec3 getPointInTriangle(const IvDevice Triangle & triangle, Vec3 bary);

Vec3 getPointInTriangle(const IvDevice Triangle & triangle, Vec2 weights);


IVES_GI_NAMESPACE_END

#endif//Triangle_h
