#include "Scene.hpp"
#include "RandomGen.hpp"
#include "SpatialIndex.hpp"
#include "Material.hpp"

using namespace IvES::GI;

namespace
{
   const unsigned int MAX_EMITTERS = 16;
}

Scene::Scene(const std::vector<Triangle> & triangles,
             const std::vector<Material> & materials,
             const Vec3 & eyePosition,
             const Vec3 & skyEmission,
             const Vec3 & groundReflection)
    : mTriangles(triangles),
      mMaterials(materials),
      mSkyEmission(skyEmission),
      mGroundReflection(groundReflection)
{
    mSkyEmission = simd::clamp(mSkyEmission, Vec3(0.0), mSkyEmission);
    mGroundReflection = mSkyEmission * simd::clamp(mGroundReflection, Vec3(0.0), Vec3(1.0));

    for (size_t i = 0; (i < mTriangles.size()) && (mEmitterTriangleIndices.size() < (1u << MAX_EMITTERS)); i++)
    {
        const Material & mat = mMaterials[mTriangles[i].materialIndex];
        if (!simd::equal(mat.emissive, Vec3(0.0)) &&
            (mTriangles[i].area > 0.0))
        {
            mEmitterTriangleIndices.push_back(static_cast<unsigned int>(i));
        }
    }
    
    construct_spatial_index(mSpatialIndex, mTriangleIndexArray, eyePosition, triangles);
}

void Scene::getIntersection(const Ray & ray,
                            const uint32_t lastHitIndex,
                            uint32_t & hitIndex,
                            Vec3 & hitPosition) const
{
    get_intersection(mSpatialIndex, mTriangles, mTriangleIndexArray, 0, ray, lastHitIndex, hitIndex, hitPosition);
}

void Scene::getEmitter(RandomGen & random,
                       Vec3 & position,
                       uint32_t & emitterIndex) const
{
    if (!mEmitterTriangleIndices.empty())
    {
        const unsigned long index = ((getRandomUInt(random) &
                                     ((1u << MAX_EMITTERS) - 1u)) * mEmitterTriangleIndices.size()) >> MAX_EMITTERS;
        emitterIndex = mEmitterTriangleIndices[index];
        const Triangle & emitter = mTriangles[emitterIndex];
        position = getSamplePoint(emitter, random);
    }
    else
    {
        position = Vec3(0.0);
        emitterIndex = NO_INDEX;
    }
}


int Scene::getEmittersCount() const
{
    return static_cast<int>(mEmitterTriangleIndices.size());
}

Vec3 Scene::getDefaultEmission(const Vec3& backDirection) const
{
    return (backDirection[1] < 0.0) ? mSkyEmission : mGroundReflection;
}
