//
//  DirectRayTracer.cpp
//  RayTracer
//
//  Created by Peder Blekken on 5/27/18.
//  Copyright © 2018 Peder Blekken. All rights reserved.
//

#include "DirectRayTracer.h"
#include "Scene.hpp"
#include "SurfacePoint.hpp"
#include "Camera.hpp"
#include "Image.hpp"
#include "RandomGen.hpp"
#include "Material.hpp"

IVES_GI_NAMESPACE_BEGIN

static Vec3 normalizeColor(const Vec3 & c)
{
    Float max = simd::max(c[0], simd::max(c[1], c[2]));
    if (max > 1.0)
    {
        return c / max;
    }
    return c;
}

static Vec3 sampleEmitters(const Scene & scene,
                           const Vec3 & rayDirection,
                           const SurfacePoint & surfacePoint,
                           RandomGen & random)
{
    Vec3 color(0);
    // Vec3 emitterPosition(0);
    
    const size_t NUM_EMITTER_SAMPLES = 2;
    const std::vector<unsigned int> & emitterTriangleIndices = scene.emitterTrianglesIndices();
    
    //    const Float weight = 1.0 / (NUM_EMITTER_SAMPLES);
    const Vec3 diffuse = surfacePoint.material.diffuse;
    
    for (size_t i = 0; i < emitterTriangleIndices.size(); i++)
    {
        const uint32_t emitterIndex = emitterTriangleIndices[i];
        const Triangle* emitter = &scene.triangles()[emitterIndex];
        for (size_t j = 0; j < NUM_EMITTER_SAMPLES; j++)
        {
            Vec3 emitterPosition = getSamplePoint(*emitter, random);
            const Vec3 emitDirection(simd::normalize(emitterPosition -
                                                     surfacePoint.position));
            uint32_t hitIndex = NO_INDEX;
            Vec3 hitPosition;
            scene.getIntersection(make_ray(surfacePoint.position, emitDirection),
                                   surfacePoint.hitIndex, hitIndex, hitPosition);
            
            // Vec3 emissionIn;
            if (emitterIndex == hitIndex)
            {
                const Triangle & hitTriangle = scene.triangles()[hitIndex];
                Float dot = simd::dot(surfacePoint.normal, emitDirection);
                if (dot > 0.0)
                {
                    Float dot1 = simd::dot(emitDirection, -hitTriangle.normal);
                    dot *= dot1;
                    if (dot > 0.0)
                    {
                        const Float spotExp = 1.0;
                        dot *= simd::pow(dot, spotExp);
                        color += diffuse * dot;
#if 1
                        Vec3 halfVec = simd::normalize(emitDirection -  rayDirection);
                        Float specDot = simd::dot(surfacePoint.normal, halfVec);
                        if (specDot > 0.0)
                        {
                            const Float shininess = 64.0;
                            color += diffuse * simd::pow(specDot, shininess) * dot1;
                        }
#endif
                    }
                }
            }
        }
    }
    return color;
}

static Vec3 getColor(const Scene & scene,
                     const Float ambience,
                     const Ray & ray,
                     RandomGen & random)
{
    Vec3 hitPosition;
    uint32_t hitIndex = NO_INDEX;
    scene.getIntersection(ray, NO_INDEX, hitIndex, hitPosition);
    
    Vec3 color;
    if (NO_INDEX != hitIndex)
    {
        const Triangle & hitTriangle = scene.triangles()[hitIndex];
        const Material & material = scene.materials()[hitTriangle.materialIndex];
        SurfacePoint surfacePoint = make_surface_point(hitPosition,
                                                       hitTriangle.normal,
                                                       hitTriangle.tangent,
                                                       material,
                                                       hitTriangle.area,
                                                       hitIndex);
        
        Vec3 emissive = normalizeColor(material.emissive);
        color = material.diffuse * ambience + emissive;
        color += sampleEmitters(scene, ray.direction, surfacePoint, random);
    }
    else
    {
        color = scene.getDefaultEmission(-ray.direction);
    }
    return color;
}

void render_frame(const Scene & scene,
                  const Float ambience,
                  const Camera & camera,
                  RandomGen & random,
                  Image<Vec3> & image,
                  unsigned int frameNum)
{
    const unsigned int width = image.size()[0];
    const unsigned int height = image.size()[1];
    const Float widthF = static_cast<Float>(width);
    const Float heightF = static_cast<Float>(height);
    
    for (unsigned int y = 0; y < height; y++)
    {
        for (unsigned int x = 0; x < width; x++)
        {
            const Float xF = ((static_cast<Float>(x) + getRandomFloat(random)) * 2.0 / widthF) - 1.0;
            const Float yF = ((static_cast<Float>(y) + getRandomFloat(random)) * 2.0 / heightF) - 1.0;
            const Vec3 offset((camera.right * xF) + (camera.up * yF * (heightF / widthF)));
            Vec3 sampleDirection = simd::normalize(camera.viewDirection + (offset * tan(camera.viewAngle * 0.5)));
            Vec3 color(getColor(scene, ambience, make_ray(camera.viewPosition, sampleDirection), random));
#if 0
            image.pixel(MakeUInt2(x, y)) =
            (image.pixel(MakeUInt2(x,y)) * static_cast<Float>(frameNum) + color) /
            (static_cast<Float>(frameNum+1));
#else
            image.pixel(MakeUInt2(x, y)) += color;
#endif
        }
    }
}

IVES_GI_NAMESPACE_END
