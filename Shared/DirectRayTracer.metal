//
//  DirectRayTracer.metal
//  RayTracer
//
//  Created by Peder Blekken on 6/10/18.
//  Copyright © 2018 Peder Blekken. All rights reserved.
//

#include <metal_stdlib>

#include "Basic.hpp"
#include "RayTracerState.h"
#include "Camera.hpp"
#include "Ray.hpp"
#include "Box.hpp"
#include "SpatialIndex.hpp"
#include "Material.hpp"
#include "SurfacePoint.hpp"
#include "RandomGen.hpp"

using namespace metal;

using namespace IvES::GI;

struct Scene {
    const device Triangle * triangles;
    const device Material * materials;
    
    const device uint * emitterTriangleIndices;
    uint numEmitters;
    
    float3 skyEmission;
    float3 groundReflection;
    
    const device SpatialIndexNode * spatialIndex;
    const device uint * triangleIndexArray;
};

Vec3 normalizeColor(const Vec3 c)
{
    Float max = simd::max(c[0], simd::max(c[1], c[2]));
    if (max > 1.0)
    {
        return c / max;
    }
    return c;
}

inline Vec3 getDefaultEmission(const thread Scene & scene,
                               const Vec3 backDirection)
{
    return (backDirection[1] < 0.0) ? scene.skyEmission : scene.groundReflection;
}

inline void getIntersection(const thread Scene & scene,
                            Ray ray,
                            const uint32_t lastHitIndex,
                            thread uint32_t & hitIndex,
                            thread Vec3 & hitPosition)
{
    Vec3 start(NAN);
    get_intersection(scene.spatialIndex, scene.triangles, scene.triangleIndexArray, 0, ray, lastHitIndex, hitIndex, hitPosition, start);
}

Vec3 sampleDirectEmitters(const thread Scene & scene,
                          const Vec3 rayDirection,
                          const thread SurfacePoint & surfacePoint,
                          thread RandomGen & random)
{
    Vec3 color(0);
    // Vec3 emitterPosition(0);
    
    const size_t NUM_EMITTER_SAMPLES = 2;
    const device unsigned int * emitterTriangleIndices = scene.emitterTriangleIndices;
    
    //    const Float weight = 1.0 / (NUM_EMITTER_SAMPLES);
    const Vec3 diffuse = surfacePoint.material.diffuse;
    
    for (unsigned int i = 0; i < scene.numEmitters; i++)
    {
        const uint32_t emitterIndex = emitterTriangleIndices[i];
        const device Triangle & emitter = scene.triangles[emitterIndex];
        for (unsigned int j = 0; j < NUM_EMITTER_SAMPLES; j++)
        {
            Vec3 emitterPosition = getSamplePoint(emitter, random);
            const Vec3 emitDirection = simd::normalize(emitterPosition - surfacePoint.position);
            uint32_t hitIndex = NO_INDEX;
            Vec3 hitPosition;
            getIntersection(scene, make_ray(surfacePoint.position, emitDirection),
                            surfacePoint.hitIndex, hitIndex, hitPosition);
            
            if (emitterIndex == hitIndex)
            {
                const device Triangle & hitTriangle = scene.triangles[hitIndex];
                Float dotVal = simd::dot(surfacePoint.normal, emitDirection);
                if (dotVal > 0.0)
                {
                    Float dot1 = simd::dot(emitDirection, -hitTriangle.normal);
                    dotVal *= dot1;
                    if (dotVal > 0.0)
                    {
                        const Float spotExp = 1.0;
                        dotVal *= simd::pow(dotVal, spotExp);
                        color += diffuse * dotVal;
#if 0
                        Vec3 halfVec = simd::normalize(emitDirection -  rayDirection);
                        Float specDot = simd::dot(surfacePoint.normal, halfVec);
                        if (specDot > 0.0)
                        {
                            const Float shininess = 64.0;
                            color += diffuse * simd::pow(specDot, shininess) * dot1;
                        }
#endif
                    }
                }
            }
        }
    }
    return color;
}

Vec3 getColor(const thread Scene & scene,
              const Float ambience,
              const Ray ray,
              thread RandomGen & random)
{
    Vec3 hitPosition;
    uint32_t hitIndex = NO_INDEX;
    getIntersection(scene, ray, NO_INDEX, hitIndex, hitPosition);
    
    Vec3 color;
    if (NO_INDEX != hitIndex)
    {
        const device Triangle & hitTriangle = scene.triangles[hitIndex];
        const device Material & material = scene.materials[hitTriangle.materialIndex];
        SurfacePoint surfacePoint = make_surface_point(hitPosition,
                                                       hitTriangle.normal,
                                                       hitTriangle.tangent,
                                                       material,
                                                       hitTriangle.area,
                                                       hitIndex);
        
        Vec3 emissive = normalizeColor(material.emissive);
        color = material.diffuse * ambience + emissive;
        color += sampleDirectEmitters(scene, ray.direction, surfacePoint, random);
    }
    else
    {
        color = getDefaultEmission(scene, -ray.direction);
    }
    return color;
}

kernel void directKernel(texture2d<float, access::read_write> texture [[texture(0)]],
                         constant RayTracerState & rayTracerState [[ buffer(0) ]],
                         const device Triangle * triangles [[ buffer(1) ]],
                         const device Material * materials [[ buffer(2) ]],
                         const device uint * emitterTriangleIndices [[ buffer(3) ]],
                         const device SpatialIndexNode * spatialIndex [[ buffer(4)]],
                         const device uint * triangleIndexArray [[ buffer(5) ]],
                         ushort2 gid [[thread_position_in_grid]])
{
    ushort width = texture.get_width();
    ushort height = texture.get_height();
    
    if (gid.x >= width || gid.y >= height) {
        return;
    }
    
    float2 position = float2(gid);
    float2 size(width, height);
    float2 tc = (position + float2(0.5)) / size;
    
    RandomGen random = makeRandomGen();
    random.seed0 ^= rayTracerState.randomSeed0;
    random.seed1 ^= rayTracerState.randomSeed1;
    
    uint tcadjust = static_cast<uint>(tc.x * 521288629.0f + tc.y * 362436069.0f);
    random.seed0 ^= tcadjust;
    random.seed1 ^= tcadjust;
    
    const Float xF = ((position.x + getRandomFloat(random)) / size.x) * 2.0 - 1.0;
    const Float yF = ((position.y + getRandomFloat(random)) / size.y) * 2.0 - 1.0;
    
    const Vec3 offset = (rayTracerState.camera.right * xF) + (rayTracerState.camera.up * yF * (size.y / size.x));
    
    float3 sampleDirection = normalize(rayTracerState.camera.viewDirection + (offset * tan(rayTracerState.camera.viewAngle * 0.5)));
    
    Ray ray = make_ray(rayTracerState.camera.viewPosition, sampleDirection);
    
    Scene scene;
    scene.triangleIndexArray = triangleIndexArray;
    scene.triangles = triangles;
    scene.materials = materials;
    scene.spatialIndex = spatialIndex;
    scene.emitterTriangleIndices = emitterTriangleIndices;
    scene.numEmitters = rayTracerState.numEmitters;
    scene.skyEmission = rayTracerState.skyEmission;
    scene.groundReflection = rayTracerState.groundReflection;
    
    Vec3 radiance = getColor(scene, 0.3, ray, random);
    
    float3 src_color = texture.read(gid).rgb * rayTracerState.frameNum;
    radiance = (src_color + radiance) / (float)(rayTracerState.frameNum+1);
    texture.write(float4(radiance, 1.0f), gid);
}
