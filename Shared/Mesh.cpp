#include "Mesh.hpp"
#include "Material.hpp"

using namespace IvES::GI;

Mesh::Mesh()
{
    // push a dummy material first
    mMaterials.push_back(make_material(Vec3(0), Vec3(0)));
}

void
Mesh::addTriangle(const Vec3 & v0,
                  const Vec3 & v1,
                  const Vec3 & v2,
                  const Material & tmpmat)
{
    unsigned int materialIndex = 0;
    mMaterials[0] = tmpmat;
    matkey key(mMaterials, 0);
    std::set<matkey>::const_iterator it = mMaterialSet.find(key);
    if (it != mMaterialSet.end())
    {
        materialIndex = (*it).mIndex;
    }
    else
    {
        materialIndex = static_cast<unsigned int>(mMaterials.size());
        mMaterials.push_back(tmpmat);
        mMaterialSet.insert(matkey(mMaterials, materialIndex));
    }
    mTriangles.push_back(make_triangle(v0,v1,v2,materialIndex));
}

///////////////////////////////////////////////////////////////////////////////////////

matkey::matkey(const std::vector<Material> &materials, unsigned int index)
    : mMaterials(materials),
      mIndex(index)
{
}
      

int matkey::operator<(const matkey & theOther) const
{
    const Material & mat = this->material();
    for (int i = 0; i < 3; i++)
    {
        float d = mat.diffuse[i] - theOther.material().diffuse[i];
        if (d < 0.0) { return 1; }
        d = mat.emissive[i] - theOther.material().emissive[i];
        if (d < 0.0) { return 1; }
    }
    return 0;
}

int matkey::operator==(const matkey & theOther) const
{
    return
    simd::equal(this->material().diffuse, theOther.material().diffuse) &&
    simd::equal(this->material().emissive, theOther.material().emissive);
}
