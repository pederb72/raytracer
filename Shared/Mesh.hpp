#ifndef IVES_GI_MESH_HPP
#define IVES_GI_MESH_HPP

#include "Basic.hpp"
#include "Triangle.hpp"
#include "Material.hpp"
#include <vector>
#include <set>

IVES_GI_NAMESPACE_BEGIN

class Material;

class matkey
{
public:
    matkey(const std::vector<Material> &materials, unsigned int index);
    const std::vector<Material> & mMaterials;
    const Material & material() const { return mMaterials[mIndex]; }
    unsigned int mIndex;
    int operator<(const matkey & theOther) const;
    int operator==(const matkey & theOther) const;
};

class Mesh
{
public:
    Mesh();

    void addTriangle(const Vec3 & v0,
                     const Vec3 & v1,
                     const Vec3 & v2,
                     const Material & mat);

    const std::vector<Triangle> & triangles() const { return mTriangles; }
    const std::vector<Material> & materials() const { return mMaterials; }

private:
    std::vector<Triangle> mTriangles;
    std::vector<Material> mMaterials;
    std::set<matkey> mMaterialSet;
};

IVES_GI_NAMESPACE_END

#endif // IVES_GI_MESH_HPP
