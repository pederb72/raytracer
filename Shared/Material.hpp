#ifndef IVES_GI_MATERIAL_HPP
#define IVES_GI_MATERIAL_HPP

#import "Basic.hpp"

IVES_GI_NAMESPACE_BEGIN

struct Material {
    Vec3 diffuse;
    Vec3 emissive;
};

#if !__METAL_VERSION__
Material make_material(const Vec3 & diffuse, const Vec3 & emissive);
#endif

IVES_GI_NAMESPACE_END

#endif // IVES_GI_MATERIAL_HPP

