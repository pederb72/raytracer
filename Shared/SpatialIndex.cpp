#include "SpatialIndex.hpp"
#include "Triangle.hpp"

#if !__METAL_VERSION__
using std::vector;
#endif

IVES_GI_NAMESPACE_BEGIN

#if !__METAL_VERSION__

namespace
{
    const int MAX_LEVELS = 44;
    const unsigned int MAX_ITEMS = 8;
    const Float TOLERANCE = 1.0 / 1024.0;

    Box3 triangleBounds(const Triangle & t)
    {
        Box3 box = make_box3(t.v0);
        expand(box, t.v1);
        expand(box, t.v2);
        return box;
    }
}

static void construct(std::vector<SpatialIndexNode> & indexArray,
                      uint32_t nodeIndex,
                      const vector<uint32_t> & items,
                      std::vector<uint32_t> & triangleIndexArray,
                      const std::vector<Triangle> & triangles,
                      const int level)
{
    SpatialIndexNode & node = indexArray[nodeIndex];
    node.isBranch = (items.size() > MAX_ITEMS) && (level < (MAX_LEVELS - 1));
    
    if (node.isBranch)
    {
        const Vec3 center = boxCenter(node.bounds);
        for (size_t s = 0, badSplits = 0; s < 8; s++)
        {
            Vec3 sMin, sMax;
            for (int i = 0; i < 3; i++)
            {
                sMin[i] = (s&(1<<i)) ? center[i] : node.bounds.min[i];
                sMax[i] = (s&(1<<i)) ? node.bounds.max[i] : center[i];
            }
            Box3 subBounds = make_box3(sMin, sMax);
            vector<uint32_t> subItems;
            
            for (size_t i = 0; i < items.size(); i++)
            {
                uint32_t idx = items[i];
                const Triangle & triangle = triangles[idx];
                Box3 itemBounds = triangleBounds(triangle);
                if (intersects(itemBounds, subBounds))
                {
                    subItems.push_back(idx);
                }
            }
            if (subItems.size() == items.size())
            {
                badSplits++;
            }
            
            if (!subItems.empty()) {
                const int nextLevel =
                (badSplits > 1) || ((subBounds.max[0] - subBounds.min[0]) < TOLERANCE * 4.0) ? MAX_LEVELS : level+1;
            
                SpatialIndexNode subNode = make_spatial_index_node();
                subNode.bounds = subBounds;
                uint32_t subNodeIndex = (uint32_t)indexArray.size();
                indexArray[nodeIndex].children[s] = subNodeIndex;
                indexArray.push_back(subNode);
                construct(indexArray, subNodeIndex, subItems, triangleIndexArray, triangles, nextLevel);
            }
        }
    }
    else {
        indexArray[nodeIndex].triangleStartIndex = (uint32_t)triangleIndexArray.size();
        indexArray[nodeIndex].triangleCount = (uint32_t)items.size();
        for (size_t i = 0; i < items.size(); i++) {
            triangleIndexArray.push_back(items[i]);
        }
    }
}

SpatialIndexNode make_spatial_index_node() {
    SpatialIndexNode node;
    node.isBranch = false;
    memset(node.children, 0, 8*sizeof(uint32_t));
    node.triangleStartIndex = 0;
    node.triangleCount = 0;
    
    return node;
}

void construct_spatial_index(std::vector<SpatialIndexNode> & index,
                             std::vector<uint32_t> & triangleIndexArray,
                             const Vec3 & eyePosition,
                             const std::vector<Triangle> & items)
{
    std::vector<uint32_t> indices;
    indices.reserve(items.size());
    Box3 bounds = make_box3(eyePosition);
    
    for (size_t i = 0; i < items.size(); i++)
    {
        Box3 itemBounds = triangleBounds(items[i]);
        if (isEmpty(itemBounds)) {
            continue;
        }

        // hack to avoid the sun in the spatial index
        if (fabs(itemBounds.min.x) > 100000 || fabs(itemBounds.min.y) > 100000 || fabs(itemBounds.min.z) > 100000) {
            continue;
        }
        
        indices.push_back(static_cast<uint32_t>(i));
        expand(bounds, itemBounds.min);
        expand(bounds, itemBounds.max);
    }
    
    Vec3 diag = bounds.max - bounds.min;
    Float maxSize = simd::max(diag[0], simd::max(diag[1], diag[2]));
    
    SpatialIndexNode root = make_spatial_index_node();
    root.bounds = make_box3(bounds.min, bounds.min + Vec3(maxSize));
    
    index.push_back(root);
    construct(index, 0, indices, triangleIndexArray, items, 0);
}

#endif //  !__METAL_VERSION__

struct SpatialStackItem {
    Vec3 cellPosition;
    unsigned int nodeIndex;
    size_t subCell;
};

SpatialStackItem make_spatial_stack_item(Vec3 cellPt, unsigned int idx, size_t subCell)
{
    return (SpatialStackItem){cellPt, idx, subCell};
}

void get_intersection(const IvDevice SpatialIndexNode * indexArray,
                      const IvDevice Triangle * triangles,
                      const IvDevice unsigned int * triangleIndexArray,
                      unsigned int nodeIndex,
                      Ray ray,
                      unsigned int lastHitIndex,
                      IvThread unsigned int & hitIndex_out,
                      IvThread Vec3 & hitPosition_out,
                      Vec3 startIn)
{
    SpatialStackItem stack[44];
    unsigned int stackIndex = 0;
    size_t subCell = NO_INDEX;
    unsigned int hitIndex = NO_INDEX;
    Vec3 hitPosition(NAN);
    Float nearestDistance = MAXFLOAT;
    Vec3 cellPosition = !isnan(startIn[0]) ? startIn : ray.origin;

    while (stackIndex || nodeIndex != NO_INDEX) {
        if (nodeIndex == NO_INDEX) {
            SpatialStackItem item = stack[--stackIndex];
            nodeIndex = item.nodeIndex;
            cellPosition = item.cellPosition;
            subCell = item.subCell;
        }

        const IvDevice SpatialIndexNode & node = indexArray[nodeIndex];

        if (simd::distance(cellPosition, ray.origin) > nearestDistance) {
            break;
        }

        if (node.isBranch)
        {
            Vec3 center = boxCenter(node.bounds);
            if (subCell == NO_INDEX) {
                subCell = 0;
                for (int i = 0; i < 3; i++)
                {
                    subCell |= (cellPosition[i] >= center[i]) << i;
                }
            }
            else {
                int axis = 2;
                Vec3 step;
                for (int i = 3; i-- > 0; axis = step[i] < step[axis] ? i : axis)
                {
                    const int high = (subCell >> i) & 1;
                    const Float face = (ray.direction[i] < 0.0) ^ high ?
                        (high ? node.bounds.max[i] : node.bounds.min[i]) : center[i];
                    step[i] = (face - ray.origin[i]) / ray.direction[i];
                }
                if (((subCell >> axis) & 1) ^ (ray.direction[axis] < 0.0)) {
                    nodeIndex = NO_INDEX; // process next node on stack
                    continue;
                }
                cellPosition = ray.origin + (ray.direction * step[axis]);
                subCell = subCell ^ (1 << axis);
            }
            
            unsigned int subNodeIndex = node.children[subCell];
            if (subNodeIndex) {
                stack[stackIndex++] = make_spatial_stack_item(cellPosition, nodeIndex, subCell);
                nodeIndex = subNodeIndex;
                subCell = NO_INDEX;
            }
        }
        else
        {
            for(uint32_t i = 0;  i < node.triangleCount; i++)
            {
                uint32_t triangleIndex = triangleIndexArray[node.triangleStartIndex+i];
                if (triangleIndex != lastHitIndex)
                {
                    const IvDevice Triangle & item = triangles[triangleIndex];
                    Float distance = MAXFLOAT;
                    if (getIntersection(item, ray, distance) && (distance < nearestDistance))
                    {
                        const Vec3 hit = ray.origin + (ray.direction * distance);
                        const Float t = 1.0f / 1024.0f;
                        if ((node.bounds.min[0] - hit[0] <= t) & (hit[0] - node.bounds.max[0] <= t) &
                            (node.bounds.min[1] - hit[1] <= t) & (hit[1] - node.bounds.max[1] <= t) &
                            (node.bounds.min[2] - hit[2] <= t) & (hit[2] - node.bounds.max[2] <= t))
                        {
                            hitIndex = triangleIndex;
                            nearestDistance = distance;
                            hitPosition = hit;
                        }
                    }
                }
            }
            nodeIndex = NO_INDEX; // process next node on stack
        }
    }
    
    hitIndex_out = hitIndex;
    hitPosition_out = hitPosition;
}

#if !__METAL_VERSION__

void get_intersection(const std::vector<SpatialIndexNode> & index,
                      const std::vector<Triangle> & triangles,
                      const std::vector<uint32_t> & triangleIndexArray,
                      uint32_t nodeIndex,
                      const Ray & ray,
                      const uint32_t lastHitIndex,
                      uint32_t & hitIndex,
                      Vec3 & hitPosition,
                      const Vec3 * start)
{
    Vec3 s = start ? *start : Vec3(NAN);
    get_intersection(&index[0], &triangles[0], &triangleIndexArray[0], nodeIndex, ray, lastHitIndex, hitIndex, hitPosition, s);
    
}

#endif

IVES_GI_NAMESPACE_END
