//
//  IndirectRayTracer.cpp
//  RayTracer
//
//  Created by Peder Blekken on 5/28/18.
//  Copyright © 2018 Peder Blekken. All rights reserved.
//

#include "IndirectRayTracer.hpp"
#include "Scene.hpp"
#include "SurfacePoint.hpp"
#include "Camera.hpp"
#include "Image.hpp"
#include "RandomGen.hpp"
#include "Material.hpp"

#include <dispatch/dispatch.h>

IVES_GI_NAMESPACE_BEGIN

static Vec3 sampleEmitters(const Scene & scene,
                           const Vec3 & rayDirection,
                           const SurfacePoint & surfacePoint,
                           RandomGen & random)
{
    Vec3 radiance(0);
    Vec3 emitterPosition(0);
    uint32_t emitterIndex = NO_INDEX;
    scene.getEmitter(random, emitterPosition, emitterIndex);
    
    if (NO_INDEX != emitterIndex)
    {
        const Vec3 emitDirection(simd::normalize(emitterPosition -
                                                 surfacePoint.position));
        
        uint32_t hitIndex = NO_INDEX;
        Vec3 hitPosition;
        scene.getIntersection(make_ray(surfacePoint.position, emitDirection),
                              surfacePoint.hitIndex, hitIndex, hitPosition);
        
        Vec3 emissionIn(0);
        if ((NO_INDEX == hitIndex) || (emitterIndex == hitIndex))
        {
            const Triangle & emitter = scene.triangles()[emitterIndex];
            const Material & material = scene.materials()[emitter.materialIndex];
            SurfacePoint emitterPoint = make_surface_point(emitterPosition,
                                                           emitter.normal,
                                                           emitter.tangent,
                                                           material,
                                                           emitter.area,
                                                           emitterIndex);
            
            emissionIn = getEmission(emitterPoint, surfacePoint.position,
                                     -emitDirection, true);
        }
        
        radiance = getReflection(surfacePoint, emitDirection, emissionIn *
                                 static_cast<Float>(scene.getEmittersCount()), -rayDirection);
    }
    return radiance;
}

static Vec3 getRadiance(const Scene & scene,
                        Ray ray,
                        RandomGen & random)
{
    Vec3 hitPosition;
    unsigned int lastHitIndex = NO_INDEX;
    unsigned int hitIndex = NO_INDEX;
    scene.getIntersection(ray, lastHitIndex, hitIndex, hitPosition);
    Vec3 radiance(0.0);
    const int kMaxBounces = 2;

    if (NO_INDEX != hitIndex)
    {
        Vec3 color(1.0);
        int numBounces = 0;
        while (numBounces < kMaxBounces && hitIndex != NO_INDEX) {
            const Triangle & hitTriangle = scene.triangles()[hitIndex];
            const Material & material = scene.materials()[hitTriangle.materialIndex];
            SurfacePoint surfacePoint = make_surface_point(hitPosition,
                                                           hitTriangle.normal,
                                                           hitTriangle.tangent,
                                                           material,
                                                           hitTriangle.area,
                                                           hitIndex);
            if (numBounces == 0) {
                radiance = getEmission(surfacePoint, ray.origin, -ray.direction, false);
            }
            // add emitter sample
            radiance += color * sampleEmitters(scene, ray.direction, surfacePoint, random);
        
            Vec3 nextDirection;
            if (getNextDirection(surfacePoint, random, -ray.direction, nextDirection, color)) {
                numBounces++;
                ray = make_ray(surfacePoint.position, nextDirection);
                scene.getIntersection(ray, surfacePoint.hitIndex, hitIndex, hitPosition);
                if (hitIndex == NO_INDEX) {
                    radiance += color * scene.getDefaultEmission(-ray.direction);
                }
            }
            else {
                hitIndex = NO_INDEX;
            }
        }
    }
    else
    {
        radiance = scene.getDefaultEmission(-ray.direction);
    }
    return radiance;
}

void render_frame(const Scene & scene,
                  const Camera & camera,
                  RandomGen & random,
                  Image<Vec3>& image,
                  unsigned int frameNum)
{
    const unsigned int width = image.size()[0];
    const unsigned int height = image.size()[1];
    const Float widthF = static_cast<Float>(width);
    const Float heightF = static_cast<Float>(height);
    
    dispatch_apply(width*height, DISPATCH_APPLY_AUTO, ^(size_t idx) {
        size_t x = idx % width;
        size_t y = idx / width;
        const UInt2 index = MakeUInt2((unsigned int)x, (unsigned int)y);
        const Float xF = ((static_cast<Float>(x) + getRandomFloat(random)) * 2.0 / widthF) - 1.0;
        const Float yF = ((static_cast<Float>(y) + getRandomFloat(random)) * 2.0 / heightF) - 1.0;
        const Vec3 offset((camera.right * xF) + (camera.up * yF * (heightF / widthF)));
        Vec3 sampleDirection = simd::normalize(camera.viewDirection + (offset * tan(camera.viewAngle * 0.5)));
        Vec3 radiance(getRadiance(scene, make_ray(camera.viewPosition, sampleDirection), random));
        if (frameNum > 0) {
            radiance = (image.pixel(index)*Float(frameNum) + radiance) / Float(frameNum+1);
        }
        image.pixel(index) = radiance;
    });
}

IVES_GI_NAMESPACE_END
