//
//  EraseShader.metal
//  RayTracer
//
//  Created by Peder Blekken on 6/3/18.
//  Copyright © 2018 Peder Blekken. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

struct FragmentInOut {
    float4 position [[position]];
};

vertex FragmentInOut eraseVertex(const device packed_float2 * vertex_array [[ buffer(0) ]],
                                 unsigned int vid [[ vertex_id ]])
{
    FragmentInOut out;
    out.position = float4(vertex_array[vid], 0.0, 1.0);
    return out;
}

fragment float4 eraseFragment(FragmentInOut inFrag [[stage_in]])
{
    return float4(0.0);
}
