#include "Camera.hpp"

IVES_GI_NAMESPACE_BEGIN

Camera make_camera(const Vec3 & viewPos, const Vec3 & viewDir, Float viewAngle) {
    Camera camera;
    camera.viewPosition = viewPos;
    camera.viewDirection = viewDir;
    camera.viewAngle = viewAngle;
    camera.up = Y_AXIS;
    camera.right = simd::normalize(simd::cross(camera.up, viewDir));

    if (!simd::equal(camera.right, Vec3(0.0)))
    {
        camera.up = simd::normalize(simd::cross(viewDir, camera.right));
    }
    else
    {
        camera.up = MakeVec3(0.0, 0.0, viewDir[1] < 0.0 ? 1.0 : -1.0);
        camera.right = simd::normalize(simd::cross(camera.up, viewDir));
    }
    
    return camera;
}

IVES_GI_NAMESPACE_END
