//
//  DirectRayTracer.h
//  RayTracer
//
//  Created by Peder Blekken on 5/27/18.
//  Copyright © 2018 Peder Blekken. All rights reserved.
//

#ifndef IVES_GI_DIRECT_RAY_TRACER_HPP
#define IVES_GI_DIRECT_RAY_TRACER_HPP

#include "Basic.hpp"
#include "Image.hpp"

IVES_GI_NAMESPACE_BEGIN

class RandomGen;
class SurfacePoint;
class Scene;
class Camera;

void render_frame(const Scene & scene,
                  const Float ambient,
                  const Camera & camera,
                  RandomGen & random,
                  Image<Vec3> & image,
                  unsigned int frameNum);

IVES_GI_NAMESPACE_END

#endif // IVES_GI_DIRECT_RAY_TRACER_HPP
