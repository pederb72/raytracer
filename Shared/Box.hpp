#ifndef IVES_GI_BOX_HPP
#define IVES_GI_BOX_HPP

#include "Basic.hpp"

IVES_GI_NAMESPACE_BEGIN

struct Box3 {
    Vec3 min;
    Vec3 max;
};

#if !__METAL_VERSION__
inline Box3 make_box3() {
    return (Box3) {Vec3(FLOAT_MAX()), Vec3(-FLOAT_MAX())};
}

inline Box3 make_box3(const Vec3 & v) {
    return (Box3) {v, v};
}

inline Box3 make_box3(const Vec3 & min, const Vec3 & max) {
    return (Box3) {min, max};
}

void expand(Box3 & box, const Vec3 & v);
bool intersects(const Box3 & box1, const Box3 & box2);
inline bool isEmpty(const Box3 & box) {
    return box.min[0] > box.max[0] || simd::equal(box.min, box.max);
}
#endif

inline Vec3 boxCenter(const IvDevice Box3 & box) {
    return (box.min+box.max)*0.5;
}

IVES_GI_NAMESPACE_END

#endif // IVES_GI_BOX_HPP
