#include "Box.hpp"
#include <stdio.h>

IVES_GI_NAMESPACE_BEGIN

void expand(Box3 & box, const Vec3 & pt)
{
    box.min = simd::min(box.min, pt);
    box.max = simd::max(box.max, pt);
}

bool intersects(const Box3 & box1, const Box3 & box2)
{
    for (int i = 0; i < 3; i++)
    {
        if (box1.min[i] > box2.max[i]) return false;
        if (box1.max[i] < box2.min[i]) return false;
    }
    return true;
}

IVES_GI_NAMESPACE_END
