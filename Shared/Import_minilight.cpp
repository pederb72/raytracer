#include "Import_minilight.hpp"
#include "World.hpp"
#include "Material.hpp"
#include "Triangle.hpp"
#include <iostream>
#include <fstream>

namespace
{
    using namespace IvES::GI;
    inline std::istream& operator>>(std::istream& in, Vec3 & obj)
    {
        char  c;
        Float xyz[3];
        in >> c >> xyz[0] >> xyz[1] >> xyz[2] >> c;
        obj = MakeVec3( xyz[0], xyz[1], xyz[2] );
        return in;
    }
    const char MODEL_FORMAT_ID[] = "#MiniLight";
}

using namespace IvES::GI;

std::shared_ptr<World> IvES::GI::import_minilight(const std::string & modelFilePathname)
{
    std::ifstream modelFile(modelFilePathname.c_str());
    std::string formatId;
    modelFile >> formatId;
    if (std::string(MODEL_FORMAT_ID) != formatId )
    {
        return std::shared_ptr<World> (NULL);
    }

    // read frame iterations
    int iterations = 0;
    modelFile >> iterations;

    // create top-level rendering objects with model file
    unsigned int w, h;
    modelFile >> w >> h;

    UInt2 imageSize = MakeUInt2(w, h);
    
    Vec3 viewPos, viewDir;
    Float angle;
    modelFile >> viewPos >> viewDir >> angle;

    Vec3 skyEmission;
    Vec3 groundReflection;

    modelFile >> skyEmission >> groundReflection;

    Mesh * mesh = new Mesh;

    const Material dummyMat = make_material(Vec3(0), Vec3(0));
    mesh->addTriangle(Vec3(0), Vec3(0), Vec3(0), dummyMat);

    while (!modelFile.eof())
    {
        Vec3 v0,v1,v2,d,e;
        modelFile >> v0 >> v1 >> v2 >> d >> e;
        const Material tmpmat = make_material(d, e);
        mesh->addTriangle(v0,v1,v2,tmpmat);
    }
    const Camera camera = make_camera(viewPos, viewDir, angle * PI() / 180.0);
    modelFile.close();

    return std::shared_ptr<World> (new World(std::shared_ptr<Mesh>(mesh),
                                             camera,
                                             skyEmission,
                                             groundReflection,
                                             iterations,
                                             imageSize));
}
