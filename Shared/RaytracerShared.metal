//
//  RaytracerShared.metal
//  RayTracer
//
//  Created by Peder Blekken on 6/10/18.
//  Copyright © 2018 Peder Blekken. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include "Basic.hpp"
#include "RayTracerState.h"
#include "Camera.hpp"
#include "Ray.hpp"
#include "Box.hpp"
#include "SpatialIndex.hpp"
#include "Material.hpp"
#include "SurfacePoint.hpp"
#include "RandomGen.hpp"

#include "RandomGen.cpp"
#include "Triangle.cpp"
#include "SurfacePoint.cpp"
#include "SpatialIndex.cpp"
