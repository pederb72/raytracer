//
//  MetalRayTracer.h
//  RayTracer
//
//  Created by Peder Blekken on 6/3/18.
//  Copyright © 2018 Peder Blekken. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "World.hpp"
#import "Image.hpp"
#import <Metal/Metal.h>

#import <QuartzCore/QuartzCore.h>

NS_ASSUME_NONNULL_BEGIN

@protocol CAMetalDrawable;

@interface MetalRayTracer : NSObject

- (instancetype)initWithWorld:(std::shared_ptr<IvES::GI::World>)world device:(id<MTLDevice>)device imageMode:(BOOL)imageMode;

- (void)renderFrame;
- (void)updateImage;
- (void)copyToDrawable:(nullable id<CAMetalDrawable>)drawable;
- (void)copyToImage:(IvES::GI::Image<IvES::GI::Vec3> &)image;
- (CGImageRef)newCGImage;

@end

NS_ASSUME_NONNULL_END
