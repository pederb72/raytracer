//
//  MetalFramebuffer.h
//
//  Created by Peder Blekken on 10/24/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Metal/Metal.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreVideo/CVPixelBuffer.h>

@interface IvMetalFramebuffer : NSObject

@property (nonatomic, readonly) id<MTLTexture> colorTexture;
@property (nonatomic, readonly) CGSize size;
@property (nonatomic, readonly) id<MTLDevice> device;
@property (nonatomic, readonly) MTLPixelFormat pixelFormat;
@property (nonatomic, readonly) BOOL memoryless;
@property (nonatomic, readonly) CVPixelBufferRef pixelBuffer;
@property (nonatomic, readonly) BOOL backedByIOSurface;
@property (nonatomic, readonly) NSUInteger sampleCount;

- (instancetype)initWithSize:(CGSize)size pixelFormat:(MTLPixelFormat)pixelFormat device:(id<MTLDevice>)device memoryless:(BOOL)memoryless backedByIOSurface:(BOOL)backedByIOSurface sampleCount:(NSUInteger)sampleCount;

- (BOOL)makeNonPurgeable;
- (void)makePurgeable;

@end
