#ifndef IVES_GI_RANDOM_HPP
#define IVES_GI_RANDOM_HPP

#include "Basic.hpp"

IVES_GI_NAMESPACE_BEGIN

struct RandomGen {
    unsigned int seed0, seed1;
};

RandomGen makeRandomGen(void);
unsigned int getRandomUInt(IvThread RandomGen & gen);
Float getRandomFloat(IvThread RandomGen & gen);

IVES_GI_NAMESPACE_END

#endif // IVES_GI_RANDOM_HPP

