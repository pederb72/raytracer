#ifndef IvES_GI_RAY_HPP
#define IvES_GI_RAY_HPP

#include "Basic.hpp"

IVES_GI_NAMESPACE_BEGIN

struct Ray {
    Vec3 origin;
    Vec3 direction;
};

inline Ray make_ray(const Vec3 origin, const Vec3 direction) {
    return (Ray) {origin, direction};
}

IVES_GI_NAMESPACE_END

#endif // IvES_RAY_HPP

