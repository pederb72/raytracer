#import "Image_tonemap.hpp"

/* Ward: A Contrast Based Scalefactor For Luminance Display
 * Graphics Gems 4
 */

#include <dispatch/dispatch.h>

namespace
{
    using namespace IvES::GI;
    const Float DISPLAY_LUMINANCE_MAX = 200.0;
    const Vec3 RGB_LUMINANCE = MakeVec3(0.2126, 0.7152, 0.0722);
    const Float GAMMA_ENCODE = 0.45;

    Float calculateTonemapScaling(const std::vector<Vec3>& pixels, const uint32_t width, const uint32_t height)
    {
        Float sumOfLogs = 0.0f;
        uint32_t numSamples = 0;
        const uint32_t step = 64;
        for (uint32_t y = 0; y < height; y += step) {
            for (uint32_t x = 0; x < width; x += step) {
                uint32_t idx = y * width + x;
                const Float Y = simd::dot(pixels[idx], RGB_LUMINANCE);
                sumOfLogs += log10f(Y > 1e-4 ? Y : 1e-4);
                numSamples++;
            }
        }
        Float logMeanLuminance = ivPow(10.0, sumOfLogs / static_cast<Float>(numSamples));
        const Float a = 1.219f + ivPow(DISPLAY_LUMINANCE_MAX * 0.25, 0.4);
        const Float b = 1.219f + ivPow(logMeanLuminance, 0.4);
        return ivPow(a / b, 2.5) / DISPLAY_LUMINANCE_MAX;
    }
}

IVES_GI_NAMESPACE_BEGIN

void tonemap(const Image<Vec3> & in,
             Image<Pixel> & out)
{
    const std::vector<Vec3> & inpixels = in.pixels();
    out.resize(in.size());
    const Float tonemapScaling = calculateTonemapScaling(inpixels, in.size().x, in.size().y);
    
    for (size_t i = 0; i < inpixels.size(); i++) {
        Vec3 mapped = simd::max(Vec3(0.0), inpixels[i] * tonemapScaling);
        Vec3 gammaed = simd::pow(mapped, Vec3(GAMMA_ENCODE));
        Vec3 quantized = simd::min(Vec3(255.0), simd::floor(gammaed * 255.0 + Vec3(0.5)));
        unsigned char * dst = &out.pixels()[i][0];
        for(unsigned int c = 0; c < 3; c++) {
            *dst++ = quantized[c];
        }
        *dst++ = 255;
    };
}

void simple_tonemap(const Image<Vec3> & in,
                    Image<Pixel> & out)
{
    const std::vector<Vec3> & inpixels = in.pixels();
    out.resize(in.size());
    
    dispatch_apply(inpixels.size(), DISPATCH_APPLY_AUTO, ^(size_t i) {
        unsigned char * dst = &out.pixels()[i][0];
        Vec3 color = inpixels[i];
        color = (1.0 + color) / color;

        for(unsigned int c = 0; c < 3; c++) {
            *dst++ = simd::min(255.0f, color[c]);
        }
        *dst++ = 255;
    });
}
                   
IVES_GI_NAMESPACE_END
