//
//  Grayscale.metal
//  RayTracer
//
//  Created by Peder Blekken on 6/9/18.
//  Copyright © 2018 Peder Blekken. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

struct FragmentInOut {
    float4 position [[position]];
    float2 texcoord;
};

vertex FragmentInOut intensityLogVertex(const device packed_float2 * vertex_array [[ buffer(0) ]],
                                        unsigned int vid [[ vertex_id ]])
{
    FragmentInOut out;
    float2 v = vertex_array[vid];
    out.position = float4(v, 0.0, 1.0);
    out.texcoord = float2(0.5f) * v + float2(0.5f);
    return out;
}

fragment float grayscaleFragment(FragmentInOut inFrag [[stage_in]],
                                  texture2d<float> texture_src [[ texture(0) ]])
{
    constexpr sampler textureSampler(filter::linear);
    float3 color = texture_src.sample(textureSampler, inFrag.texcoord).rgb;
    constexpr float3 RGB_LUMINANCE = float3(0.2126, 0.7152, 0.0722);
    
    return dot(color, RGB_LUMINANCE);
}

kernel void grayscaleKernel(texture2d<float, access::sample> srcTexture [[texture(0)]],
                            texture2d<float, access::write> dstTexture [[texture(1)]],
                            ushort2 gid [[thread_position_in_grid]])
{
    ushort width = srcTexture.get_width();
    ushort height = srcTexture.get_height();
    
    if (gid.x >= width || gid.y >= height) {
        return;
    }
    
    float2 position = float2(gid);
    float2 size = float2(width, height);
    float2 tc = (position + float2(0.5)) / size;

    constexpr sampler textureSampler(filter::linear);
    constexpr float3 RGB_LUMINANCE = float3(0.2126, 0.7152, 0.0722);
    float3 color = srcTexture.sample(textureSampler, tc).rgb;

    dstTexture.write(dot(color, RGB_LUMINANCE), gid);
}
