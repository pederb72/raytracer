#include "Material.hpp"

IVES_GI_NAMESPACE_BEGIN

Material make_material(const Vec3 & diffuse,
                       const Vec3 & emissive) {
    return (Material) {diffuse, emissive};
}

IVES_GI_NAMESPACE_END
