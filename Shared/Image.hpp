#ifndef IVES_GI_IMAGE_HPP
#define IVES_GI_IMAGE_HPP

#include "Basic.hpp"
#include <vector>

IVES_GI_NAMESPACE_BEGIN

struct Pixel
{
public:
    Pixel() { data[0] = data[1] = data[2] = 0; data[3] = 255; }
    Pixel(unsigned char r, unsigned char g, unsigned char b)
    {
        data[0] = r;
        data[1] = g;
        data[2] = b;
    }
    unsigned char & operator [] (int i) { return data[i]; }
    unsigned char operator [] (int i) const { return data[i]; }
private:
    unsigned char data[4];
};

template <class Type> class Image
{
public:
    Image()
        : dataSize(0)
    {
    }
    Image(const UInt2 & s)
        : dataSize(s),
          data(s[0]*s[1])
    {
    }
    void resize(const UInt2 & s)
    {
        data.resize(s[0]*s[1]);
        dataSize = s;
    }
    const Type & pixel(const UInt2 & index) const
    {
        return data[index[1]*dataSize[0]+index[0]];
    }
    Type & pixel(const UInt2 & index)
    {
        return data[index[1]*dataSize[0]+index[0]];
    }
    const UInt2 & size() const { return dataSize; }
    const std::vector<Type> & pixels() const { return data; }
    std::vector<Type> & pixels() { return data; }

private:
    UInt2 dataSize;
    std::vector<Type> data;
};

IVES_GI_NAMESPACE_END

#endif // IVES_GI_IMAGE_HPP
