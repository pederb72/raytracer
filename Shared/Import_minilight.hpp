#ifndef IVES_GI_IMPORT_MINILIGHT_HPP
#define IVES_GI_IMPORT_MINILIGHT_HPP

#include "Basic.hpp"
#include <string>
#include <memory>

IVES_GI_NAMESPACE_BEGIN

class World;

std::shared_ptr<World> import_minilight(const std::string & filename);

IVES_GI_NAMESPACE_END

#endif // IVES_GI_IMPORT_MINILIGHT_HPP
