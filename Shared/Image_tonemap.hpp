#ifndef IVES_GI_IMAGE_TONEMAP_HPP
#define IVES_GI_IMAGE_TONEMAP_HPP

#import "Image.hpp"

IVES_GI_NAMESPACE_BEGIN

void tonemap(const Image<Vec3> & in,
             Image<Pixel> & out);

void simple_tonemap(const Image<Vec3> & in,
                    Image<Pixel> & out);

IVES_GI_NAMESPACE_END

#endif // IVES_GI_IMAGE_TONEMAP_HPP
