//
//  IndirectRayTracer.hpp
//  RayTracer
//
//  Created by Peder Blekken on 5/28/18.
//  Copyright © 2018 Peder Blekken. All rights reserved.
//

#ifndef IndirectRayTracer_hpp
#define IndirectRayTracer_hpp

#include "Basic.hpp"
#include "Image.hpp"

IVES_GI_NAMESPACE_BEGIN

class RandomGen;
class SurfacePoint;
class Scene;
class Camera;

void render_frame(const Scene & scene,
                  const Camera & camera,
                  RandomGen & random,
                  Image<Vec3> & image,
                  unsigned int frameNum);

IVES_GI_NAMESPACE_END

#endif /* IndirectRayTracer_hpp */
