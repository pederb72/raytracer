//
//  MetalRayTracer.mm
//  RayTracer
//
//  Created by Peder Blekken on 6/3/18.
//  Copyright © 2018 Peder Blekken. All rights reserved.
//

#import "MetalRayTracer.h"
#import "MetalFramebuffer.h"
#import "RandomGen.hpp"
#import "RayTracerState.h"
#import "Tonemapper.h"

#import <CoreImage/CoreImagePrivate.h>
#import <mm_malloc.h>

using namespace IvES::GI;

@implementation MetalRayTracer {
    std::shared_ptr<World> _world;
    IvMetalFramebuffer *_framebuffer;
    IvMetalFramebuffer *_tonemappedFramebuffer;
    id<MTLDevice> _device;
    id<MTLCommandQueue> _commandQueue;
    id<MTLLibrary> _defaultLibrary;
    id<MTLRenderPipelineState> _tracerPipelineState;
    id<MTLRenderPipelineState> _erasePipelineState;
    MTLPixelFormat _pixelFormat;
    id<MTLBuffer> _cachedQuadVertexBuffer;
    id<MTLBuffer> _cachedQuadTexCoordBuffer;
    RandomGen _randomGen;
    id<MTLBuffer> _triangleBuffer;
    id<MTLBuffer> _materialBuffer;
    id<MTLBuffer> _emitterTriangleBuffer;
    id<MTLBuffer> _spatialIndexBuffer;
    id<MTLBuffer> _triangleIndexBuffer;
    id<MTLComputePipelineState> _indirectComputePipelineState;
    id<MTLComputePipelineState> _directComputePipelineState;
    MTLSize _threadgroupSize;
    MTLSize _threadgroupCount;
    unsigned int _frameNum;
    Tonemapper *_tonemapper;
    CIContext *_coreImageContext;
    BOOL _imageMode;
}

- (instancetype)initWithWorld:(std::shared_ptr<World>)world device:(id<MTLDevice>)device imageMode:(BOOL)imageMode
{
    self = [super init];
    if (self) {
        _device = device;
        _randomGen = makeRandomGen();
        _world = world;
        _pixelFormat = MTLPixelFormatRGBA32Float;
        _imageMode = imageMode;
        [self setupTracer];
    }
    return self;
}

- (void)setupTracer
{
    _commandQueue = [_device newCommandQueue];
    NSBundle *bundle = [NSBundle bundleForClass:self.class];
    NSError *error;
    _defaultLibrary = [_device newDefaultLibraryWithBundle:bundle error:&error];
    if (error) {
        NSLog(@"Library error: %@", error);
    }

    UInt2 imageSize = _world.get()->imageSize();
    _framebuffer = [[IvMetalFramebuffer alloc] initWithSize:CGSizeMake(imageSize[0], imageSize[1]) pixelFormat:_pixelFormat device:_device memoryless:NO backedByIOSurface:NO sampleCount:1];
    if (_imageMode) {
        _tonemappedFramebuffer = [[IvMetalFramebuffer alloc] initWithSize:CGSizeMake(imageSize[0], imageSize[1]) pixelFormat:MTLPixelFormatBGRA8Unorm device:_device memoryless:NO backedByIOSurface:NO sampleCount:1];
    }
    _tonemapper = [[Tonemapper alloc] initWithSourceTexture:_framebuffer.colorTexture destTexture:_tonemappedFramebuffer.colorTexture library:_defaultLibrary];
    
    [self setupTracerShader];
    [self setupEraseShader];
    [self setupCachedQuad];
    [self setupWorldBuffers];
    [self setupComputeKernel];
    
    [self eraseBuffer];
}

- (void)setupTracerShader
{
    NSError *error;
    id<MTLFunction> vertexFunction = [_defaultLibrary newFunctionWithName:@"indirectVertex"];
    id<MTLFunction> fragmentFunction = [_defaultLibrary newFunctionWithName:@"indirectFragment"];
    if (!vertexFunction || !fragmentFunction) {
        @throw @"Failed to compile shader";
    }
    MTLRenderPipelineDescriptor *pipelineStateDescriptor = [[MTLRenderPipelineDescriptor alloc] init];
    pipelineStateDescriptor.label = @"Tracer pipeline State";
    pipelineStateDescriptor.vertexFunction = vertexFunction;
    pipelineStateDescriptor.fragmentFunction = fragmentFunction;
    pipelineStateDescriptor.colorAttachments[0].pixelFormat = _pixelFormat;
    
    _tracerPipelineState = [_device newRenderPipelineStateWithDescriptor:pipelineStateDescriptor error:&error];
    if (error) {
        NSLog(@"Pipeline state error: %@", error);
    }
}

- (void)setupEraseShader
{
    NSError *error;
    id<MTLFunction> vertexFunction = [_defaultLibrary newFunctionWithName:@"eraseVertex"];
    id<MTLFunction> fragmentFunction = [_defaultLibrary newFunctionWithName:@"eraseFragment"];
    if (!vertexFunction || !fragmentFunction) {
        @throw @"Failed to compile shader";
    }
    MTLRenderPipelineDescriptor *pipelineStateDescriptor = [[MTLRenderPipelineDescriptor alloc] init];
    pipelineStateDescriptor.label = @"Erase pipeline State";
    pipelineStateDescriptor.vertexFunction = vertexFunction;
    pipelineStateDescriptor.fragmentFunction = fragmentFunction;
    pipelineStateDescriptor.colorAttachments[0].pixelFormat = _pixelFormat;
    
    _erasePipelineState = [_device newRenderPipelineStateWithDescriptor:pipelineStateDescriptor error:&error];
    if (error) {
        NSLog(@"Pipeline state error: %@", error);
    }
}

- (void)setupCachedQuad
{
    if (!_cachedQuadVertexBuffer) {
        static const float vertices[] = {
            -1, -1,
            1, -1,
            -1,  1,
            1,  1,
        };
        
        id<MTLBuffer> metalBuffer = [_device newBufferWithBytes:vertices length:sizeof(vertices) options:MTLResourceCPUCacheModeDefaultCache];
        _cachedQuadVertexBuffer = metalBuffer;
    }
    
    if (!_cachedQuadTexCoordBuffer) {
        static const float texCoords[] = {
            0, 1,
            1, 1,
            0, 0,
            1, 0,
        };
        id<MTLBuffer> metalBuffer = [_device newBufferWithBytes:texCoords length:sizeof(texCoords) options:MTLResourceCPUCacheModeDefaultCache];
        _cachedQuadTexCoordBuffer = metalBuffer;
    }
}

- (void)setupComputeKernel
{
    _threadgroupSize = MTLSizeMake(16, 16, 1);
    _threadgroupCount.width  = (static_cast<size_t>(_framebuffer.size.width)  + _threadgroupSize.width -  1) / _threadgroupSize.width;
    _threadgroupCount.height = (static_cast<size_t>(_framebuffer.size.height) + _threadgroupSize.height - 1) / _threadgroupSize.height;
    _threadgroupCount.depth = 1;

    {
        id<MTLFunction> kernelFunction = [_defaultLibrary newFunctionWithName:@"indirectKernel"];
        
        NSError *error = nil;
        _indirectComputePipelineState = [_device newComputePipelineStateWithFunction:kernelFunction
                                                                               error:&error];
        if (error) {
            NSLog(@"Indirect compute error: %@", error);
        }
    }
    if (0) {
        id<MTLFunction> kernelFunction = [_defaultLibrary newFunctionWithName:@"directKernel"];
        
        NSError *error = nil;
        _directComputePipelineState = [_device newComputePipelineStateWithFunction:kernelFunction
                                                                             error:&error];
        if (error) {
            NSLog(@"Diret compute error: %@", error);
        }
    }
}

- (void)renderFrame
{
    [self renderFrameCompute];
    _frameNum++;
}

- (void)updateImage
{
    [_tonemapper runInCommandQueue:_commandQueue drawable:nil];
}

- (void)copyToDrawable:(nullable id<CAMetalDrawable>)drawable
{
    [_tonemapper runInCommandQueue:_commandQueue drawable:drawable];
}

- (void)renderFrameCompute
{
    id<MTLCommandBuffer> commandBuffer = [_commandQueue commandBuffer];
    id<MTLComputeCommandEncoder> computeEncoder = [commandBuffer computeCommandEncoder];
    
    [computeEncoder setComputePipelineState:_indirectComputePipelineState];
    [computeEncoder setTexture:_framebuffer.colorTexture
                       atIndex:0];
    
    [computeEncoder setBuffer:_triangleBuffer offset:0 atIndex:1];
    [computeEncoder setBuffer:_materialBuffer offset:0 atIndex:2];
    [computeEncoder setBuffer:_emitterTriangleBuffer offset:0 atIndex:3];
    [computeEncoder setBuffer:_spatialIndexBuffer offset:0 atIndex:4];
    [computeEncoder setBuffer:_triangleIndexBuffer offset:0 atIndex:5];

    const World & world = *_world.get();
    const Scene & scene = world.scene();
    const Camera & camera = world.camera();
    
    RayTracerState state;
    state.camera = camera;
    state.numEmitters = static_cast<unsigned int>(scene.emitterTrianglesIndices().size());
    state.randomSeed0 = arc4random();  //  getRandomUInt(_randomGen);
    state.randomSeed1 = arc4random();  // 
    state.imageSize = simd_make_float2(world.imageSize()[0], world.imageSize()[1]);
    state.skyEmission = scene.skyEmission();
    state.groundReflection = scene.groundReflection();
    state.frameNum = _frameNum;
    [computeEncoder setBytes:&state length:sizeof(state) atIndex:0];

    [computeEncoder dispatchThreadgroups:_threadgroupCount
                   threadsPerThreadgroup:_threadgroupSize];
    [computeEncoder endEncoding];
    [commandBuffer commit];
}

- (void)renderFrameFragment
{
    id<MTLCommandBuffer> commandBuffer = [_commandQueue commandBuffer];
    MTLRenderPassDescriptor *renderPassDescriptor = [MTLRenderPassDescriptor renderPassDescriptor];
    MTLRenderPassColorAttachmentDescriptor *destAttachment = renderPassDescriptor.colorAttachments[0];
    destAttachment.loadAction = MTLLoadActionDontCare;
    destAttachment.storeAction = MTLStoreActionStore;
    destAttachment.texture = _framebuffer.colorTexture;
    
    id<MTLRenderCommandEncoder> renderEncoder = [commandBuffer renderCommandEncoderWithDescriptor:renderPassDescriptor];
    [renderEncoder setFragmentTexture:_framebuffer.colorTexture atIndex:0];
    [renderEncoder setFragmentBuffer:_triangleBuffer offset:0 atIndex:1];
    [renderEncoder setFragmentBuffer:_materialBuffer offset:0 atIndex:2];
    [renderEncoder setFragmentBuffer:_emitterTriangleBuffer offset:0 atIndex:3];
    [renderEncoder setFragmentBuffer:_spatialIndexBuffer offset:0 atIndex:4];
    [renderEncoder setFragmentBuffer:_triangleIndexBuffer offset:0 atIndex:5];
    
    const World & world = *_world.get();
    const Scene & scene = world.scene();
    const Camera & camera = world.camera();

    RayTracerState state;
    state.camera = camera;
    state.numEmitters = static_cast<unsigned int>(scene.emitterTrianglesIndices().size());
    state.randomSeed0 = arc4random();
    state.randomSeed1 = arc4random();
    state.imageSize = simd_make_float2(world.imageSize()[0], world.imageSize()[1]);
    state.skyEmission = scene.skyEmission();
    state.groundReflection = scene.groundReflection();
    [renderEncoder setFragmentBytes:&state length:sizeof(state) atIndex:0];
    [renderEncoder setRenderPipelineState:_tracerPipelineState];
    [self renderFullscreenQuadWithRenderEncoder:renderEncoder];
    [renderEncoder endEncoding];
    [commandBuffer commit];
}

- (void)eraseBuffer
{
    id<MTLCommandBuffer> commandBuffer = [_commandQueue commandBuffer];
    MTLRenderPassDescriptor *renderPassDescriptor = [MTLRenderPassDescriptor renderPassDescriptor];
    MTLRenderPassColorAttachmentDescriptor *destAttachment = renderPassDescriptor.colorAttachments[0];
    
    destAttachment.loadAction = MTLLoadActionDontCare;
    destAttachment.storeAction = MTLStoreActionStore;
    destAttachment.texture = _framebuffer.colorTexture;
    
    id<MTLRenderCommandEncoder> renderEncoder = [commandBuffer renderCommandEncoderWithDescriptor:renderPassDescriptor];
    [renderEncoder setRenderPipelineState:_erasePipelineState];
    [self renderFullscreenQuadWithRenderEncoder:renderEncoder];
    [renderEncoder endEncoding];
    [commandBuffer commit];
}

- (void)renderFullscreenQuadWithRenderEncoder:(id<MTLRenderCommandEncoder>)renderEncoder
{
    [renderEncoder setVertexBuffer:_cachedQuadVertexBuffer offset:0 atIndex:0];
    [renderEncoder setVertexBuffer:_cachedQuadTexCoordBuffer offset:0 atIndex:1];
    
    [renderEncoder drawPrimitives:MTLPrimitiveTypeTriangleStrip vertexStart:0 vertexCount:4];
#if !TARGET_OS_IPHONE
    [renderEncoder textureBarrier];
#endif
}

- (void)setupWorldBuffers
{
    const World & world = *_world.get();
    const Scene & scene = world.scene();
    
    MTLResourceOptions resourceOptions = MTLResourceCPUCacheModeDefaultCache;
    _triangleBuffer = [_device newBufferWithBytes:&scene.triangles()[0] length:scene.triangles().size()*sizeof(Triangle) options:resourceOptions];
    _materialBuffer = [_device newBufferWithBytes:&scene.materials()[0] length:scene.materials().size()*sizeof(Material) options:resourceOptions];
    if (scene.emitterTrianglesIndices().size()) {
        _emitterTriangleBuffer = [_device newBufferWithBytes:&scene.emitterTrianglesIndices()[0] length:scene.emitterTrianglesIndices().size()*sizeof(unsigned int) options:resourceOptions];
    }
    else {
        _emitterTriangleBuffer = [_device newBufferWithLength:4 options:resourceOptions];
    }
    _spatialIndexBuffer = [_device newBufferWithBytes:&scene.spatialIndex()[0] length:scene.spatialIndex().size()*sizeof(SpatialIndexNode) options:resourceOptions];
    _triangleIndexBuffer = [_device newBufferWithBytes:&scene.triangleIndices()[0] length:scene.triangleIndices().size()*sizeof(unsigned int) options:resourceOptions];
}

- (void)copyToImage:(Image<Vec3> &)image
{
    MTLResourceOptions resourceOptions = MTLResourceCPUCacheModeDefaultCache;
    
    size_t memsize = image.pixels().size()*sizeof(Vec3);
    size_t pagesize = ceil(memsize / 4096.0) * 4096;
    void *ptr = _mm_malloc(pagesize, 4096);
    
    id<MTLBuffer> buffer = [_device newBufferWithBytesNoCopy:ptr length:pagesize options:resourceOptions deallocator:^(void * _Nonnull pointer, NSUInteger length) {
        _mm_free(ptr);
    }];
    
    id<MTLTexture> colorTexture = _framebuffer.colorTexture;
    UInt2 imageSize = _world.get()->imageSize();
    id<MTLCommandBuffer> commandBuffer = [_commandQueue commandBuffer];
    id<MTLBlitCommandEncoder> blitEncoder = [commandBuffer blitCommandEncoder];
    [blitEncoder copyFromTexture:colorTexture sourceSlice:0 sourceLevel:0 sourceOrigin:MTLOriginMake(0, 0, 0) sourceSize:MTLSizeMake(imageSize[0], imageSize[1], 1) toBuffer:buffer destinationOffset:0 destinationBytesPerRow:image.size()[0]*sizeof(Vec3) destinationBytesPerImage:image.pixels().size()*sizeof(Vec3)];
    [blitEncoder endEncoding];
    [commandBuffer addCompletedHandler:^(id<MTLCommandBuffer> _Nonnull) {
        memcpy(&image.pixels()[0], ptr, memsize);
    }];
    [commandBuffer commit];
    [commandBuffer waitUntilCompleted];
}

- (CIImage *)CIImageFromTexture:(id<MTLTexture>)texture
{
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    NSDictionary *options = @{kCIImageColorSpace : (__bridge id)colorSpace, kCIImageFlipped : @NO};
    CGColorSpaceRelease(colorSpace);
    CIImage *ciImage = [[CIImage alloc] initWithMTLTexture:texture options:options];
    return ciImage;
}

- (CGImageRef)newCGImageFromTexture:(id<MTLTexture>)texture clipRect:(CGRect)clipRect copyImage:(BOOL)copyImage
{
    CIImage *ciImage = [self CIImageFromTexture:texture];
    CGRect extent = [ciImage extent];
    BOOL isClipped = !CGRectIsNull(clipRect) && !CGRectEqualToRect(extent, clipRect);
    
    if (isClipped) {
        extent = clipRect;
        extent.origin.y = _framebuffer.colorTexture.height - CGRectGetMaxY(clipRect);
    }
    
    if (!_coreImageContext) {
        _coreImageContext = [CIContext contextWithMTLDevice:_device];
    }
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGImageRef img = [_coreImageContext createCGImage:ciImage fromRect:extent format:kCIFormatBGRA8 colorSpace:colorSpace deferred:!copyImage];
    CGColorSpaceRelease(colorSpace);
    
    return img;
}

- (CGImageRef)newCGImage
{
    return [self newCGImageFromTexture:_tonemappedFramebuffer.colorTexture clipRect:CGRectNull copyImage:YES];
}

@end
