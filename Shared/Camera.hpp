#ifndef IVES_GI_CAMERA_HPP
#define IVES_GI_CAMERA_HPP

#include "Basic.hpp"

IVES_GI_NAMESPACE_BEGIN

struct Camera {
    Vec3 viewPosition;
    Vec3 viewDirection;
    Vec3 right;
    Vec3 up;
    Float viewAngle;
};

#if !__METAL_VERSION__
Camera make_camera(const Vec3 & viewPos, const Vec3 & viewDir, Float viewAngle);
#endif

IVES_GI_NAMESPACE_END

#endif // IVES_GI_CAMERA_HPP
