//
//  RayTracerState.h
//  RayTracer
//
//  Created by Peder Blekken on 6/3/18.
//  Copyright © 2018 Peder Blekken. All rights reserved.
//

#ifndef RayTracerState_h
#define RayTracerState_h

#include "Camera.hpp"

IVES_GI_NAMESPACE_BEGIN

struct RayTracerState {
    Camera camera;
    Vec2 imageSize;
    unsigned int numEmitters;
    unsigned int randomSeed0;
    unsigned int randomSeed1;
    unsigned int frameNum;
    Vec3 skyEmission;
    Vec3 groundReflection;
};

IVES_GI_NAMESPACE_END

#endif /* RayTracerState_h */
