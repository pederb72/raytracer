#ifndef IVES_GI_WORLD_HPP
#define IVES_GI_WORLD_HPP

#include "Mesh.hpp"
#include "Camera.hpp"
#include "Scene.hpp"
#include <memory>

IVES_GI_NAMESPACE_BEGIN

class World
{
public:
    World(std::shared_ptr<Mesh> mesh,
          const Camera & camera,
          const Vec3 & skyEmission,
          const Vec3 & groundReflection,
          int iterations,
          const UInt2 & imageSize);

    const Mesh & mesh() const { return *(mMesh.get()); }
    const Camera & camera() const { return mCamera; }
    const Scene & scene() const { return mScene; }
    const int iterations() const { return mIterations; }
    const UInt2 imageSize() const { return mImageSize; }

    void updateImageSize(unsigned int width, unsigned int height);
private:
    std::shared_ptr<Mesh> mMesh;
    Camera mCamera;
    Scene mScene;
    int mIterations;
    float mAspectRatio;
    UInt2 mImageSize;
};

IVES_GI_NAMESPACE_END

#endif // IVES_GI_WORLD_HPP

