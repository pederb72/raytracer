#include "Triangle.hpp"
#include "Box.hpp"
#include "RandomGen.hpp"

IVES_GI_NAMESPACE_BEGIN

#if !__METAL_VERSION__
Vec3 calcNormal(const Triangle & t)
{
    return simd::normalize(simd::cross(t.v1 - t.v0, t.v2 - t.v1));
}

static Vec3 calcTangent(const Triangle & t)
{
    return simd::normalize(t.v1 - t.v0);
}


static Float calcArea(const Triangle & t)
{
    const Vec3 pa2 = simd::cross(t.v1 - t.v0, t.v2 - t.v1);
    return sqrt(simd::dot(pa2, pa2)) * 0.5;
}

Triangle make_triangle(const Vec3 & v0,
                       const Vec3 & v1,
                       const Vec3 & v2,
                       unsigned int materialIndex) {
    Triangle t;
    t.materialIndex = materialIndex;
    t.v0 = v0;
    t.v1 = v1;
    t.v2 = v2;
    t.normal = calcNormal(t);
    t.tangent = calcTangent(t);
    t.area = calcArea(t);

    return t;
}
#endif

bool getIntersection(const IvDevice Triangle & triangle,
                     Ray ray,
                     IvThread Float & hitDistance) {
    const Vec3 edge1 = triangle.v1 - triangle.v0;
    const Vec3 edge2 = triangle.v2 - triangle.v0;
    const Vec3 dir = ray.direction;
    Vec3 pvec = simd::cross(dir, edge2);

    // if determinant is near zero, ray lies in plane of triangle.
    Float det = simd::dot(edge1, pvec);
    if (fabs(det) < EPSILON) return false;

    Float inv_det = 1.0 / det;

    // calculate distance from v0 to ray origin
    Vec3 tvec = ray.origin - triangle.v0;

    // calculate U parameter and test bounds
    Float u = simd::dot(tvec, pvec) * inv_det;
    if (u < 0.0 || u > 1.0)
        return false;

    Vec3 qvec = simd::cross(tvec, edge1);

    // calculate V parameter and test bounds
    Float v = simd::dot(dir, qvec) * inv_det;
    if (v < 0.0 || u + v > 1.0)
        return false;

    hitDistance = simd::dot(edge2, qvec) * inv_det;
    return hitDistance >= 0.0;
}

Vec3 getPointInTriangle(const IvDevice Triangle & triangle, Vec2 weights)
{
    return Vec3(((triangle.v1 - triangle.v0) * weights[0]) +
                ((triangle.v2 - triangle.v0) * weights[1]) + triangle.v0);
}

Vec3 getPointInTriangle(const IvDevice Triangle & triangle, Vec3 bary)
{
    return triangle.v0*bary[0] + triangle.v1*bary[1] + triangle.v2*bary[2];
}

Vec3 getSamplePoint(const IvDevice Triangle & triangle, IvThread RandomGen & random)
{
    const Float sqr1 = sqrt(getRandomFloat(random));
    const Float r2 = getRandomFloat(random);
    return getPointInTriangle(triangle, MakeVec2(1.0 - sqr1, (1.0 - r2) * sqr1));
}

IVES_GI_NAMESPACE_END

