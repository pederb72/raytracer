//
//  Tonemapper.m
//  RayTracer
//
//  Created by Peder Blekken on 6/9/18.
//  Copyright © 2018 Peder Blekken. All rights reserved.
//

#import "Tonemapper.h"
#import <MetalPerformanceShaders/MetalPerformanceShaders.h>
#import <QuartzCore/CAMetalLayer.h>

@implementation Tonemapper {
    id<MTLTexture> _srcTexture;
    id<MTLTexture> _dstTexture;
    id<MTLDevice> _device;
    MTLSize _threadgroupSize;
    MTLSize _threadgroupCount;
    id<MTLComputePipelineState> _intensityLogPipelineState;
    id<MTLComputePipelineState> _tonemapPipelineState;
    id<MTLLibrary> _library;
    MPSImageIntegral *_imageIntegral;
}

- (instancetype)initWithSourceTexture:(id<MTLTexture>)src destTexture:(id<MTLTexture>)dstTexture library:(id<MTLLibrary>)library
{
    self = [super init];
    if (self) {
        _srcTexture = src;
        _dstTexture = dstTexture;
        _library = library;
        _device = src.device;
        [self setup];
    }
    return self;
}

- (void)setup
{
    _threadgroupSize = MTLSizeMake(16, 16, 1);
    _threadgroupCount.width  = (_srcTexture.width  + _threadgroupSize.width -  1) / _threadgroupSize.width;
    _threadgroupCount.height = (_srcTexture.height + _threadgroupSize.height - 1) / _threadgroupSize.height;
    _threadgroupCount.depth = 1;
    
    [self setupIntensityLogKernel];
    [self setupTonemapKernel];
}

- (void)setupIntensityLogKernel
{
    id<MTLFunction> kernelFunction = [_library newFunctionWithName:@"intensityLogKernel"];
    
    NSError *error = nil;
    _intensityLogPipelineState = [_device newComputePipelineStateWithFunction:kernelFunction
                                                                   error:&error];
    if (error) {
        NSLog(@"Intensity log error: %@", error);
    }
}

- (void)setupTonemapKernel
{
    id<MTLFunction> kernelFunction = [_library newFunctionWithName:@"tonemapKernel"];
    
    NSError *error = nil;
    _tonemapPipelineState = [_device newComputePipelineStateWithFunction:kernelFunction
                                                                    error:&error];
    if (error) {
        NSLog(@"Tonemap error: %@", error);
    }
}

- (id<MTLTexture>)grayscaleTexture
{
    MTLTextureDescriptor *desc = [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:MTLPixelFormatR32Float
                                                                                    width:64
                                                                                   height:64
                                                                                mipmapped:NO];
    desc.textureType = MTLTextureType2D;
    desc.usage = MTLTextureUsageShaderWrite | MTLTextureUsageShaderRead;
    desc.storageMode = MTLStorageModePrivate;

    return [_device newTextureWithDescriptor:desc];
}

- (void)runInCommandQueue:(id<MTLCommandQueue>)commandQueue drawable:(id<CAMetalDrawable>)drawable
{
    id<MTLTexture> grayscaleTexture = [self grayscaleTexture];
    [self computeIntensityLogInTexture:grayscaleTexture commandQueue:commandQueue];
    id<MTLTexture> integralTexture = [self integrateTexture:grayscaleTexture commandQueue:commandQueue];
    
    id<MTLCommandBuffer> commandBuffer = [commandQueue commandBuffer];
    id<MTLComputeCommandEncoder> computeEncoder = [commandBuffer computeCommandEncoder];
    [computeEncoder setComputePipelineState:_tonemapPipelineState];
    [computeEncoder setTexture:_srcTexture
                       atIndex:0];
    if (drawable) {
        [computeEncoder setTexture:[drawable texture]
                           atIndex:1];
    }
    else {
        [computeEncoder setTexture:_dstTexture
                           atIndex:1];
    }
    [computeEncoder setTexture:integralTexture
                       atIndex:2];
    [computeEncoder dispatchThreadgroups:_threadgroupCount
                   threadsPerThreadgroup:_threadgroupSize];
    [computeEncoder endEncoding];
    if (drawable) {
        [commandBuffer presentDrawable:drawable];
    }
    [commandBuffer commit];
    // [commandBuffer waitUntilCompleted];
}

- (void)computeIntensityLogInTexture:(id<MTLTexture>)grayscaleTexture commandQueue:(id<MTLCommandQueue>)commandQueue
{
    id<MTLCommandBuffer> commandBuffer = [commandQueue commandBuffer];
    id<MTLComputeCommandEncoder> computeEncoder = [commandBuffer computeCommandEncoder];
    
    [computeEncoder setComputePipelineState:_intensityLogPipelineState];
    [computeEncoder setTexture:_srcTexture
                       atIndex:0];
    [computeEncoder setTexture:grayscaleTexture atIndex:1];
    [computeEncoder dispatchThreadgroups:_threadgroupCount
                   threadsPerThreadgroup:_threadgroupSize];
    [computeEncoder endEncoding];
    [commandBuffer commit];
    // [commandBuffer waitUntilCompleted];
}

- (id<MTLTexture>)integrateTexture:(id<MTLTexture>)grayscaleTexture commandQueue:(id<MTLCommandQueue>)commandQueue
{
    id<MTLCommandBuffer> commandBuffer = [commandQueue commandBuffer];
    id<MTLTexture> integrateTexture = [self grayscaleTexture];
    MPSImageIntegral *integralKernel = [[MPSImageIntegral alloc] initWithDevice:_device];
    integralKernel.offset = (MPSOffset){0,0,0};
    [integralKernel encodeToCommandBuffer:commandBuffer sourceTexture:grayscaleTexture destinationTexture:integrateTexture];
    [commandBuffer commit];
    // [commandBuffer waitUntilCompleted];
    return integrateTexture;
}

@end
