//
//  IndirectRayTracer.metal
//  RayTracer
//
//  Created by Peder Blekken on 5/28/18.
//  Copyright © 2018 Peder Blekken. All rights reserved.
//

#include "Basic.hpp"
#include "RayTracerState.h"
#include "Camera.hpp"
#include "Ray.hpp"
#include "Box.hpp"
#include "SpatialIndex.hpp"
#include "Material.hpp"
#include "SurfacePoint.hpp"
#include "RandomGen.hpp"

using namespace metal;

using namespace IvES::GI;

struct Scene {
    const device Triangle * triangles;
    const device Material * materials;
    
    const device uint * emitterTriangleIndices;
    uint numEmitters;
    
    float3 skyEmission;
    float3 groundReflection;
    
    const device SpatialIndexNode * spatialIndex;
    const device uint * triangleIndexArray;
};

void getEmitter(const thread Scene & scene,
                thread RandomGen & random,
                thread Vec3 & position,
                thread unsigned int & emitterIndex)
{
    if (scene.numEmitters) {
        const unsigned int MAX_EMITTERS = 16;
        const unsigned int index = ((getRandomUInt(random) &
                                      ((1u << MAX_EMITTERS) - 1u)) * scene.numEmitters) >> MAX_EMITTERS;
        emitterIndex = scene.emitterTriangleIndices[index];
        const device Triangle & emitter = scene.triangles[emitterIndex];
        position = getSamplePoint(emitter, random);
    }
    else
    {
        position = Vec3(0.0);
        emitterIndex = NO_INDEX;
    }
}

inline void getIntersection(const thread Scene & scene,
                            Ray ray,
                            const uint32_t lastHitIndex,
                            thread uint32_t & hitIndex,
                            thread Vec3 & hitPosition)
{
    Vec3 start(NAN);
    get_intersection(scene.spatialIndex, scene.triangles, scene.triangleIndexArray, 0, ray, lastHitIndex, hitIndex, hitPosition, start);
}

Vec3 sampleEmitters(const thread Scene & scene,
                    const Vec3 rayDirection,
                    const thread SurfacePoint & surfacePoint,
                    thread RandomGen & random)
{
    Vec3 radiance(0);
    Vec3 emitterPosition(0);
    uint32_t emitterIndex = NO_INDEX;
    getEmitter(scene, random, emitterPosition, emitterIndex);
    
    if (NO_INDEX != emitterIndex)
    {
        const Vec3 emitDirection = simd::normalize(emitterPosition -
                                                   surfacePoint.position);
        
        uint32_t hitIndex = NO_INDEX;
        Vec3 hitPosition;
        getIntersection(scene, make_ray(surfacePoint.position, emitDirection),
                        surfacePoint.hitIndex, hitIndex, hitPosition);
        
        Vec3 emissionIn(0);
        if ((NO_INDEX == hitIndex) || (emitterIndex == hitIndex))
        {
            const device Triangle & emitter = scene.triangles[emitterIndex];
            const device Material & material = scene.materials[emitter.materialIndex];
            SurfacePoint emitterPoint = make_surface_point(emitterPosition,
                                                           emitter.normal,
                                                           emitter.tangent,
                                                           material,
                                                           emitter.area,
                                                           emitterIndex);
            
            emissionIn = getEmission(emitterPoint, surfacePoint.position,
                                     -emitDirection, true);
        }
        
        radiance = getReflection(surfacePoint, emitDirection, emissionIn *
                                 static_cast<Float>(scene.numEmitters), -rayDirection);
    }
    return radiance;
}

inline Vec3 getDefaultEmission(const thread Scene & scene,
                               const Vec3 backDirection)
{
    return (backDirection[1] < 0.0) ? scene.skyEmission : scene.groundReflection;
}

Vec3 getRadiance(const thread Scene & scene,
                 Ray ray,
                 thread RandomGen & random)
{
    Vec3 hitPosition;
    unsigned int lastHitIndex = NO_INDEX;
    unsigned int hitIndex = NO_INDEX;
    getIntersection(scene, ray, lastHitIndex, hitIndex, hitPosition);
    Vec3 radiance = Vec3(0.0);

    const int kMaxBounces = 3;
    
    if (NO_INDEX != hitIndex)
    {
        Vec3 color = Vec3(1.0);
        int numBounces = 0;
        while (numBounces < kMaxBounces && hitIndex != NO_INDEX) {
            const device Triangle & hitTriangle = scene.triangles[hitIndex];
            const device Material & material = scene.materials[hitTriangle.materialIndex];
            SurfacePoint surfacePoint = make_surface_point(hitPosition,
                                                           hitTriangle.normal,
                                                           hitTriangle.tangent,
                                                           material,
                                                           hitTriangle.area,
                                                           hitIndex);
            if (numBounces == 0) {
                radiance = getEmission(surfacePoint, ray.origin, -ray.direction, false);
            }
            numBounces++;
            // add emitter sample
            radiance += color * sampleEmitters(scene, ray.direction, surfacePoint, random);
            
            Vec3 nextDirection;
            if (getNextDirection(surfacePoint, random, -ray.direction, nextDirection, color)) {
                ray = make_ray(surfacePoint.position, nextDirection);
                getIntersection(scene, ray, surfacePoint.hitIndex, hitIndex, hitPosition);
                if (hitIndex == NO_INDEX) {
                    radiance += color * getDefaultEmission(scene, -ray.direction);
                }
            }
            else {
                hitIndex = NO_INDEX;
            }
        }
    }
    else
    {
        radiance = getDefaultEmission(scene, -ray.direction);
    }
    return radiance;
}

struct FragmentInOut {
    float4 position [[position]];
    float2 texcoord;
};

vertex FragmentInOut indirectVertex(const device packed_float2 * vertex_array [[ buffer(0) ]],
                                    const device packed_float2 * texcoord_array [[ buffer(1) ]],
                                    unsigned int vid [[ vertex_id ]])
{
    FragmentInOut out;
    out.position = float4(vertex_array[vid], 0.0, 1.0);
    out.texcoord = float2(texcoord_array[vid]);
    return out;
}

fragment float4 indirectFragment(FragmentInOut inFrag [[stage_in]],
                                texture2d<float> texture_src [[ texture(0) ]],
                                constant RayTracerState & rayTracerState [[ buffer(0) ]],
                                const device Triangle * triangles [[ buffer(1) ]],
                                const device Material * materials [[ buffer(2) ]],
                                const device uint * emitterTriangleIndices [[ buffer(3) ]],
                                const device SpatialIndexNode * spatialIndex [[ buffer(4)]],
                                const device uint * triangleIndexArray [[ buffer(5) ]])

                                
{
    float2 tc = inFrag.texcoord;
    float2 size = rayTracerState.imageSize;
    RandomGen random = makeRandomGen();
    random.seed0 ^= rayTracerState.randomSeed0;
    random.seed1 ^= rayTracerState.randomSeed1;
    
    uint tcadjust = static_cast<uint>(tc.x * 521288629.0f + tc.y * 362436069.0f);
    random.seed0 ^= tcadjust;
    random.seed1 ^= tcadjust;

    const Float xF = (tc.x + getRandomFloat(random) / size.x) * 2.0 - 1.0;
    const Float yF = (tc.y + getRandomFloat(random) / size.y) * 2.0 - 1.0;
    
    const Vec3 offset = (rayTracerState.camera.right * xF) + (rayTracerState.camera.up * yF * (size.y / size.x));

    float3 sampleDirection = normalize(rayTracerState.camera.viewDirection + (offset * tan(rayTracerState.camera.viewAngle * 0.5)));

    Ray ray = make_ray(rayTracerState.camera.viewPosition, sampleDirection);

    Scene scene;
    scene.triangleIndexArray = triangleIndexArray;
    scene.triangles = triangles;
    scene.materials = materials;
    scene.spatialIndex = spatialIndex;
    scene.emitterTriangleIndices = emitterTriangleIndices;
    scene.numEmitters = rayTracerState.numEmitters;
    scene.skyEmission = rayTracerState.skyEmission;
    scene.groundReflection = rayTracerState.groundReflection;
    
    Vec3 radiance = getRadiance(scene, ray, random);
    // radiance = Vec3(tc.x*2, tc.y*2, 0);
    constexpr sampler textureSampler(filter::nearest);
    float3 src_color = texture_src.sample(textureSampler, tc).rgb;
    // src_color = float3(0.0);
    
    return float4(radiance + src_color, 1.0f);
}

kernel void indirectKernel(texture2d<float, access::read_write> texture [[texture(0)]],
                           constant RayTracerState & rayTracerState [[ buffer(0) ]],
                           const device Triangle * triangles [[ buffer(1) ]],
                           const device Material * materials [[ buffer(2) ]],
                           const device uint * emitterTriangleIndices [[ buffer(3) ]],
                           const device SpatialIndexNode * spatialIndex [[ buffer(4)]],
                           const device uint * triangleIndexArray [[ buffer(5) ]],
                           ushort2 gid [[thread_position_in_grid]])
{
    ushort width = texture.get_width();
    ushort height = texture.get_height();
    
    if (gid.x >= width || gid.y >= height) {
        return;
    }
    
    float2 position = float2(gid);
    float2 size(width, height);
    float2 tc = (position + float2(0.5)) / size;

    RandomGen random = makeRandomGen();
    random.seed0 ^= rayTracerState.randomSeed0;
    random.seed1 ^= rayTracerState.randomSeed1;
    
    uint tcadjust = static_cast<uint>(tc.x * 521288629.0f + tc.y * 362436069.0f);
    random.seed0 ^= tcadjust;
    random.seed1 ^= tcadjust;
    
    const Float xF = ((position.x + getRandomFloat(random)) / size.x) * 2.0 - 1.0;
    const Float yF = ((position.y + getRandomFloat(random)) / size.y) * 2.0 - 1.0;
    
    const Vec3 offset = (rayTracerState.camera.right * xF) + (rayTracerState.camera.up * yF * (size.y / size.x));
    
    float3 sampleDirection = normalize(rayTracerState.camera.viewDirection + (offset * tan(rayTracerState.camera.viewAngle * 0.5)));
    
    Ray ray = make_ray(rayTracerState.camera.viewPosition, sampleDirection);
    
    Scene scene;
    scene.triangleIndexArray = triangleIndexArray;
    scene.triangles = triangles;
    scene.materials = materials;
    scene.spatialIndex = spatialIndex;
    scene.emitterTriangleIndices = emitterTriangleIndices;
    scene.numEmitters = rayTracerState.numEmitters;
    scene.skyEmission = rayTracerState.skyEmission;
    scene.groundReflection = rayTracerState.groundReflection;
    
    Vec3 radiance = getRadiance(scene, ray, random);
    // radiance = Vec3(tc.x*2, tc.y*2, 0);

    float3 src_color = texture.read(gid).rgb * rayTracerState.frameNum;
    radiance = (src_color + radiance) / (float)(rayTracerState.frameNum+1);
    texture.write(float4(radiance, 1.0f), gid);
}
