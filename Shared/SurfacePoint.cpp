#include "SurfacePoint.hpp"
#include "RandomGen.hpp"
#include "Triangle.hpp"

IVES_GI_NAMESPACE_BEGIN

SurfacePoint make_surface_point(const Vec3 position,
                                const Vec3 normal,
                                const Vec3 tangent,
                                const Material material,
                                const Float area,
                                const uint32_t hitIndex) {
    SurfacePoint sp;
    sp.position = position;
    sp.normal = normal;
    sp.tangent = tangent;
    sp.material = material;
    sp.area = area;
    sp.hitIndex = hitIndex;

    return sp;
}

Vec3 getEmission(const IvThread SurfacePoint & sp,
                 const IvThread Vec3 & toPosition,
                 const IvThread Vec3 & outDirection,
                 const bool isSolidAngle)
{
    const Vec3 rayDir = toPosition - sp.position;
    const Float distance2 = simd::dot(rayDir, rayDir);
    const Float cosArea = simd::dot(outDirection, sp.normal) * sp.area;

    // with infinity clamped out
    const Float solidAngle = isSolidAngle ?
        cosArea / (distance2 >= EPSILON ? distance2 : EPSILON) : 1.0;

    // emit from front face of surface only
    return Vec3(cosArea > 0.0 ? sp.material.emissive * solidAngle : Vec3(0.0));
}

Vec3 getReflection(const IvThread SurfacePoint & sp,
                   const IvThread Vec3 & inDirection,
                   const IvThread Vec3 & inRadiance,
                   const IvThread Vec3 & outDirection)
{
    const Float inDot = simd::dot(inDirection, sp.normal);
    const Float outDot = simd::dot(outDirection, sp.normal);

    // directions must be on same side of surface (no transmission)
    return ((inDot < 0.0) ^ (outDot < 0.0)) ? Vec3(0.0) :
                 // ideal diffuse BRDF:
                 // radiance scaled by reflectivity, cosine, and 1/pi
                 (inRadiance * sp.material.diffuse) * (fabs( inDot ) / M_PI_F);
}


bool getNextDirection(const IvThread SurfacePoint & sp,
                      IvThread RandomGen & random,
                      const IvThread Vec3 & inDirection,
                      IvThread Vec3 & outDirection_o,
                      IvThread Vec3 & color_o)
{
    outDirection_o = Vec3(0.0);

    const Float reflectivityMean =
        simd::dot(sp.material.diffuse, Vec3(1.0)) / 3.0;

    // russian-roulette for reflectance magnitude
    if (getRandomFloat(random) < reflectivityMean)
    {
        // cosine-weighted importance sample hemisphere
        const Float _2pr1 = M_PI_F * 2.0 * getRandomFloat(random);
        const Float sr2 = sqrt(getRandomFloat(random));

        // make coord frame coefficients (z in normal direction)
        const Float x = cos( _2pr1 ) * sr2;
        const Float y = sin( _2pr1 ) * sr2;
        const Float z = sqrt( 1.0f - (sr2 * sr2) );

        // make coord frame
        Vec3 normal = sp.normal;
        const Vec3 tangent = sp.tangent;
        // put normal on inward ray side of surface (preventing transmission)
        normal = simd::dot(normal, inDirection ) >= 0.0f ? normal : -normal;

        // make vector from frame scaled by coefficients
        outDirection_o = (tangent * x) + (simd::cross(normal, tangent ) * y) +
            (normal * z);

        // make color by dividing-out mean from reflectivity
        color_o = sp.material.diffuse / reflectivityMean;
    }
    return !simd::all(outDirection_o == Vec3(0.0));
}

IVES_GI_NAMESPACE_END

