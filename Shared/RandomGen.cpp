#if !__METAL_VERSION__
#include "RandomGen.hpp"
#endif

IVES_GI_NAMESPACE_BEGIN

RandomGen makeRandomGen(void)
{
    RandomGen gen;
    gen.seed0 = 521288629u;
    gen.seed1 = 362436069u;
    return gen;
}

unsigned int getRandomUInt(IvThread RandomGen & gen)
{
    gen.seed0 = 18000u * (gen.seed0 & 0xFFFFu) + (gen.seed0 >> 16);
    gen.seed1 = 30903u * (gen.seed1 & 0xFFFFu) + (gen.seed1 >> 16);
    
    return (gen.seed0 << 16) + (gen.seed1 & 0xFFFFu);
}

Float getRandomFloat(IvThread RandomGen & gen)
{
    return static_cast<Float>(getRandomUInt(gen)) / 4294967296.0;
}

IVES_GI_NAMESPACE_END
