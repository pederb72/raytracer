//
//  Tonemapper.h
//  RayTracer
//
//  Created by Peder Blekken on 6/9/18.
//  Copyright © 2018 Peder Blekken. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Metal/Metal.h>

@protocol CAMetalDrawable;

NS_ASSUME_NONNULL_BEGIN

@interface Tonemapper : NSObject

- (instancetype)initWithSourceTexture:(id<MTLTexture>)src destTexture:(nullable id<MTLTexture>)dstTexture library:(id<MTLLibrary>)library;
- (void)runInCommandQueue:(id<MTLCommandQueue>)commandQueue drawable:(nullable id<CAMetalDrawable>)drawable;

@end

NS_ASSUME_NONNULL_END
