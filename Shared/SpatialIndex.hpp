#ifndef IVES_GI_SPATIALINDEX_HPP
#define IVES_GI_SPATIALINDEX_HPP

#include "Basic.hpp"
#include "Triangle.hpp"
#include "Box.hpp"
#if !__METAL_VERSION__
#include <vector>
#else
using metal::uint32_t;
#endif

IVES_GI_NAMESPACE_BEGIN

struct SpatialIndexNode {
    bool isBranch;
    Box3 bounds;
    uint32_t children[8];
    uint32_t triangleStartIndex, triangleCount;
};

#if !__METAL_VERSION__
SpatialIndexNode make_spatial_index_node();
void construct_spatial_index(std::vector<SpatialIndexNode> & indexArray,
                             std::vector<uint32_t> & triangleIndexArray,
                             const Vec3 & eyePosition,
                             const std::vector<Triangle> & items);

void get_intersection(const std::vector<SpatialIndexNode> & index,
                      const std::vector<Triangle> & triangles,
                      const std::vector<uint32_t> & triangleIndexArray,
                      uint32_t nodeIndex,
                      const Ray & ray,
                      const uint32_t lastHitIndex,
                      uint32_t & hitIndex,
                      Vec3 & hitPosition,
                      const Vec3 * start = 0);
#endif

void get_intersection(const IvDevice SpatialIndexNode * indexArray,
                      const IvDevice Triangle * triangles,
                      const IvDevice unsigned int * triangleIndexArray,
                      unsigned int nodeIndex,
                      Ray ray,
                      unsigned int lastHitIndex,
                      IvThread unsigned int & hitIndex_out,
                      IvThread Vec3 & hitPosition_out,
                      Vec3 startIn);

IVES_GI_NAMESPACE_END

#endif // IVES_GI_SPATIALINDEX_HPP
