//
//  Tonemap.metal
//  RayTracer
//
//  Created by Peder Blekken on 6/9/18.
//  Copyright © 2018 Peder Blekken. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

kernel void intensityLogKernel(texture2d<float, access::sample> srcTexture [[texture(0)]],
                               texture2d<float, access::write> dstTexture [[texture(1)]],
                               ushort2 gid [[thread_position_in_grid]])
{
    ushort width = srcTexture.get_width();
    ushort height = srcTexture.get_height();
    
    if (gid.x >= width || gid.y >= height) {
        return;
    }
    
    float2 position = float2(gid);
    float2 size = float2(width, height);
    float2 tc = (position + float2(0.5)) / size;
    
    constexpr sampler textureSampler(filter::linear);
    constexpr float3 RGB_LUMINANCE = float3(0.2126, 0.7152, 0.0722);
    float3 color = srcTexture.sample(textureSampler, tc).rgb;
    
    float intensity = dot(color, RGB_LUMINANCE);
    float logVal = log10(max(intensity, 1e-4));
    dstTexture.write(logVal, gid);
}


kernel void tonemapKernel(texture2d<float, access::read> srcTexture [[texture(0)]],
                          texture2d<half, access::write> dstTexture [[texture(1)]],
                          texture2d<float, access::read> integralTexture [[texture(2)]],
                          ushort2 gid [[thread_position_in_grid]])
{
    ushort width = srcTexture.get_width();
    ushort height = srcTexture.get_height();
    
    if (gid.x >= width || gid.y >= height) {
        return;
    }
    
    constexpr float DISPLAY_LUMINANCE_MAX = 200.0;
    constexpr float GAMMA_ENCODE = 0.45;
    const uint2 integralSize = uint2(integralTexture.get_width(), integralTexture.get_height());
    const float sumOfLogs = integralTexture.read(integralSize - uint2(1)).r;
    const float logMeanLuminance = pow(10.0, sumOfLogs / static_cast<float>(integralSize.x * integralSize.y));
    const float a = 1.219f + pow(DISPLAY_LUMINANCE_MAX * 0.25, 0.4);
    const float b = 1.219f + pow(logMeanLuminance, 0.4);
    const float tonemapScaling = pow(a / b, 2.5) / DISPLAY_LUMINANCE_MAX;

    float3 mapped = max(float3(0.0), srcTexture.read(gid).rgb * tonemapScaling);
    float3 gammaed = min(1.0, pow(mapped, float3(GAMMA_ENCODE)));
    
    dstTexture.write(half4(half3(gammaed), 1.0h), uint2(gid));
}
