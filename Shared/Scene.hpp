#ifndef IVES_GI_SCENE_HPP
#define IVES_GI_SCENE_HPP

#include "Basic.hpp"
#include "Ray.hpp"
#include "Triangle.hpp"
#include "SpatialIndex.hpp"
#include "Material.hpp"

#include <vector>

IVES_GI_NAMESPACE_BEGIN

class RandomGen;

class Scene
{
public:
    Scene(const std::vector<Triangle> & triangles,
          const std::vector<Material> & materials,
          const Vec3 & eyePosition,
          const Vec3 & skyEmission,
          const Vec3 & groundReflection);

    void getIntersection(const Ray & ray,
                         const uint32_t lastHitIndex,
                         uint32_t & hitIndex,
                         Vec3 & hitPosition) const;

    void getEmitter(RandomGen & random,
                    Vec3 & position,
                    uint32_t & emitterIndex) const;
    int getEmittersCount() const;
    const std::vector<unsigned int> & emitterTrianglesIndices() const { return mEmitterTriangleIndices; }
    const std::vector<Triangle> & triangles() const { return mTriangles; }
    const std::vector<Material> & materials() const { return mMaterials; }
    const std::vector<SpatialIndexNode> & spatialIndex() const { return mSpatialIndex; }
    const std::vector<unsigned int> & triangleIndices() const { return mTriangleIndexArray; }
    const Vec3 & skyEmission() const { return mSkyEmission; }
    const Vec3 & groundReflection() const { return mGroundReflection; }
    Vec3 getDefaultEmission(const Vec3 & backDirection) const;

private:
    const std::vector<Triangle> & mTriangles;
    const std::vector<Material> & mMaterials;
    std::vector<unsigned int> mEmitterTriangleIndices;

    Vec3 mSkyEmission;
    Vec3 mGroundReflection;
    
    std::vector<SpatialIndexNode> mSpatialIndex;
    std::vector<uint32_t> mTriangleIndexArray;
};

IVES_GI_NAMESPACE_END

#endif // IVES_GI_SCENE_HPP
