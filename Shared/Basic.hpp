#ifndef IvES_Basic_hpp
#define IvES_Basic_hpp

#define IVES_GI_NAMESPACE_BEGIN namespace IvES { namespace GI {
#define IVES_GI_NAMESPACE_END }}

#if !__METAL_VERSION__
#include <simd/simd.h>
#include <cmath>
#include <limits>
#define IvThread
#define IvDevice
#else
#define IvThread thread
#define IvDevice device
#include <metal_stdlib>
#endif

#define NO_INDEX ((uint32_t) 0xffffffffu)
#define EPSILON 1.0e-6
#define M_PI_F 3.14159265358979323846264338327950288f
#if !__METAL_VERSION__
// #define MAXFLOAT std::numeric_limits<Float>::max()
#endif

IVES_GI_NAMESPACE_BEGIN

#if __METAL_VERSION__
typedef float Float;
typedef metal::float3 Vec3;
typedef metal::float2 Vec2;
typedef metal::uint2 UInt2;
#define MakeVec2(_x_, _y) float2(_x_, _y)
#define MakeVec3(_x_, _y_, _z_) float3(_x_, _y_, _z_)

#else
typedef unsigned int HitObjectIdentifier;
typedef simd_float3 Vec3;
typedef simd_float2 Vec2;
typedef simd_uint2 UInt2;
typedef float Float;
inline Float FLOAT_MAX() { return std::numeric_limits<Float>::max(); }
inline Float sqrt(Float x) { return std::sqrt(x); }
inline Float cos(Float x) { return std::cos(x); }
inline Float sin(Float x) { return std::sin(x); }
inline Float tan(Float x) { return std::tan(x); }
inline Float ivPow(Float x, Float y) { return std::pow(x,y); }
inline Float log10(Float x) { return std::log10(x); }
inline Float floor(Float x) { return std::floor(x); }
inline Float ceil(Float x) { return std::ceil(x); }
inline Float PI() { return 3.14159265358979323846264338327950288; }
inline Float fabs(Float x) { return std::fabs(x); }
#define X_AXIS simd_make_float3(1.0, 0.0, 0.0)
#define Y_AXIS simd_make_float3(0.0, 1.0, 0.0)
#define Z_AXIS simd_make_float3(0.0, 0.0, 1.0)
#define MakeUInt2(_x_, _y_) simd_make_uint2(_x_, _y_)
#define MakeVec2(_x_, _y) simd_make_float2(_x_, _y)
#define MakeVec3(_x_, _y_, _z_) simd_make_float3(_x_, _y_, _z_)
#endif

IVES_GI_NAMESPACE_END

#endif // IvES_Basic_hpp
