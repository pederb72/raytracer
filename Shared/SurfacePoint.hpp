#ifndef IVES_SURFACEPOINT_HPP
#define IVES_SURFACEPOINT_HPP

#include "Basic.hpp"
#include "Material.hpp"

IVES_GI_NAMESPACE_BEGIN

class RandomGen;
class Material;

struct SurfacePoint {
    Vec3 position;
    Vec3 normal;
    Vec3 tangent;
    Material material;
    Float area;
    uint32_t hitIndex;
};

SurfacePoint make_surface_point(const Vec3 position,
                                const Vec3 normal,
                                const Vec3 tangent,
                                const Material material,
                                const Float area,
                                const uint32_t hitIndex);

Vec3 getEmission(const IvThread SurfacePoint & sp,
                 const IvThread Vec3 & toPosition,
                 const IvThread Vec3 & outDirection,
                 const bool isSolidAngle);

Vec3 getReflection(const IvThread SurfacePoint & sp,
                   const IvThread Vec3 & inDirection,
                   const IvThread Vec3 & inRadiance,
                   const IvThread Vec3 & outDirection);

bool getNextDirection(const IvThread SurfacePoint & sp,
                      IvThread RandomGen & random,
                      const IvThread Vec3 & inDirection,
                      IvThread Vec3 & outDirection,
                      IvThread Vec3 & color);

IVES_GI_NAMESPACE_END

#endif // IVES_SURFACEPOINT_HPP
